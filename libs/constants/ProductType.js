const { PRODUCTTYPE, PACKAGE } = require("./enums");
const PRODUCT_TYPES = [
  { label: "Website Templates", value: PRODUCTTYPE.Website },
  { label: "Hosting Packages", value: PRODUCTTYPE.Hosting },
  { label: "Device Packages", value: PRODUCTTYPE.Device },
  { label: "Video Packages", value: PRODUCTTYPE.Video },
  { label: "Design Packages", value: PRODUCTTYPE.Design }
];

const PACK_AGE = [
  { label: "3 Months", value: PACKAGE.ThreeMonth },
  { label: "6 Months", value: PACKAGE.SixMonth },
  { label: "12 Months", value: PACKAGE.OneYear }
];

module.exports = { PRODUCT_TYPES, PACK_AGE };
