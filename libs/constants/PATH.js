//#region đường dẫn path thư mục statics templates
const PATH_TEMPLATE_STATICS = "statics/templates";
const { TEMPLATE_PATH_NAME } = require("../../configs/template");
//#endregion
//#region đường dẫn path thư mục  templates
const PATH_TEMPLATE = "templates/statics/";
const PATH_TEMPLATE_NAME = `${TEMPLATE_PATH_NAME}/`;
const PATH_TEMPLATE_GLOBAL = "global/";
const PATH_TEMPLATE_PAGE = "pages/";
const PATH_TEMPLATE_RELEASE = "release/";
//#endregion
module.exports = {
  PATH_TEMPLATE_GLOBAL,
  PATH_TEMPLATE,
  PATH_TEMPLATE_NAME,
  PATH_TEMPLATE_PAGE,
  PATH_TEMPLATE_RELEASE
};
