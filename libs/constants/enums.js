const THEME_TEXT_TYPE = {
  Normal: 0,
  Html: 1,
  Link: 2
};
const PRODUCTTYPE = {
  Website: 0,
  Device: 1,
  Hosting: 2,
  Video: 3,
  Design: 4
};
const PAYMENT_STATUS = {
  NotPaid: 0,
  Paid: 1
};
const PAYMENT_METHOD = {
  COD: 0,
  CreditCard: 1
};
const ORDER_STATUS = {
  New: 0,
  Deployed: 1,
  Complete: 2,
  Cancel: 3
};

const PACKAGE = {
  ThreeMonth: 0,
  SixMonth: 1,
  OneYear: 2
};
const THEMETYPE = {
  Group: 0,
  Image: 1,
  Text: 2,
  ImageList: 3,
  TextImageList: 4
};
const BOOKINGTYPE = {
  Individual: 0,
  Group: 1
};

const GENDERTYPE = {
  Undefine: 0,
  Male: 1,
  Female: 2
};
const WEBSITE_STATUS = {
  Inactive: 1,
  Active: 2,
  Warning: 3
};
const DEVICE_STATUS = {
  Inactive: 1,
  Active: 2,
  Warning: 3
};
module.exports = {
  THEMETYPE,
  THEME_TEXT_TYPE,
  BOOKINGTYPE,
  GENDERTYPE,
  PRODUCTTYPE,
  ORDER_STATUS,
  PACKAGE,
  PAYMENT_METHOD,
  PAYMENT_STATUS,
  WEBSITE_STATUS,
  DEVICE_STATUS
};
