const ROLE_NAME = {
  Member : "Member",
  Manager: "Manager",
  Administrator: "Administrator",
  Developer : "Developer"
};
const ROLE_ID = {
  Member: 0,
  Manager: 1,
  Administrator: 2,
  Developer : 3
};
const ROLE_MANAGER_MEMBER = [
  {value: ROLE_ID.Member, label: ROLE_NAME.Member},
];
const ROLE_MANAGER_ADMIN = [
  {value: ROLE_ID.Manager, label: ROLE_NAME.Manager},
  {value: ROLE_ID.Administrator, label: ROLE_NAME.Administrator}
];
const ROLE_MANAGER_DEVELOPER = [
  {value: ROLE_ID.Developer, label: ROLE_NAME.Developer},
];
module.exports = { ROLE_ID,ROLE_NAME, ROLE_MANAGER_ADMIN, ROLE_MANAGER_MEMBER, ROLE_MANAGER_DEVELOPER };
