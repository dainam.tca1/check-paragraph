//#region Thông báo
const GLOBAL_ERROR_MESSAGE =
  "System having technical problems please try again";
const PARAMETER_INVALID = "The parameter is not properly formatted";
const DATA_UPDATE_SUCCESSFULL = "Update data successfully";
const DATA_UPDATE_IMG = "Successful photo upload";
const DATA_NOTFOUND = "No data found";
const DATA_NOT_RECOVER = "Deleted data cannot be recovered";
const DATA_CHANGE_PLAYLIST = "Are you sure all device play list";
const DATA_NOT_SELECT = "Please select the data to delete";
const DATA_LOADING_SUCCESSFULL = "Data is loading successfully";
//#endregion
//#region validate CategoryBlog
const VALID_TITLE_CATE = "Please enter a category name";
const VALID_URL = "Please enter the URL";
const VALID_SELECT_CATE = "Please select a category";
const VALID_URL_EXIST = "URL already exists please try again";
const VALID_PRICE_SERVICE = "Please enter the service price";
const VALID_TITLE_SERVICE = "Please enter the service name";
const VALID_TIME_SERVICE = "Please enter service time";
const VALID_TIME = "The parameter is not properly formatted";
const VALID_PRICE = "The parameter is not properly formatted";
const VALID_NAME = "Please enter your first and last name";
const VALID_SELECT_SERVICE = "Please choose a major";
//#endregion
//#region Text UI
const ERROR = "Error";
const SUCCESS = "Success";
const NOTI = "Notification";
//#endregion

//#region Message giỏ hàng
const QUANTITY_MORETHAN_ZERO = "The number must be greater than 0";
//#endregion
module.exports = {
  VALID_NAME,
  VALID_SELECT_SERVICE,
  VALID_TIME,
  VALID_PRICE,
  VALID_TITLE_SERVICE,
  VALID_PRICE_SERVICE,
  VALID_SELECT_CATE,
  VALID_TIME_SERVICE,
  GLOBAL_ERROR_MESSAGE,
  PARAMETER_INVALID,
  DATA_NOT_RECOVER,
  DATA_NOT_SELECT,
  DATA_UPDATE_SUCCESSFULL,
  DATA_UPDATE_IMG,
  DATA_LOADING_SUCCESSFULL,
  VALID_URL_EXIST,
  VALID_URL,
  VALID_TITLE_CATE,
  DATA_NOTFOUND,
  ERROR,
  SUCCESS,
  NOTI,
  QUANTITY_MORETHAN_ZERO,
  DATA_CHANGE_PLAYLIST
};
