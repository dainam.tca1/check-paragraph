//#region ConfigKey
const BOOKING_SETTING = "BOOKING_SETTING";
const SEO_SETTING = "SEO_SETTING";
//#endregion

module.exports = {
  BOOKING_SETTING,
  SEO_SETTING 
};
