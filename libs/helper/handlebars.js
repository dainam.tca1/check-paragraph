import moment from "moment";
import {ORDER_STATUS, PAYMENT_METHOD, PAYMENT_STATUS} from "../constants/enums";
import MenuConfig from "../../modules/menuconfig-module/models/schema/menuconfig";
exports.section = function(name, options) {
    if (!this._sections) this._sections = {};
    this._sections[name] = options.fn(this);
    return null;
},
exports.for =  function (from, to, incr, block) {
    var accum = "";
    for (var i = from; i < to; i += incr) accum += block.fn(i);
    return accum;
},
exports.compare = function (lvalue, rvalue, options){
    if (arguments.length < 3)
      throw new Error("Handlerbars Helper 'compare' needs 2 parameters");

    var operator = options.hash.operator || "==";

    var operators = {
      "==": function(l, r) {
        return l == r;
      },
      "===": function(l, r) {
        return l === r;
      },
      "!=": function(l, r) {
        return l != r;
      },
      "<": function(l, r) {
        return l < r;
      },
      ">": function(l, r) {
        return l > r;
      },
      "<=": function(l, r) {
        return l <= r;
      },
      ">=": function(l, r) {
        return l >= r;
      },
      typeof: function(l, r) {
        return typeof l == r;
      }
    };

    if (!operators[operator])
      throw new Error(
        "Handlerbars Helper 'compare' doesn't know the operator " + operator
      );

    var result = operators[operator](lvalue, rvalue);

    if (result) {
      return options.fn(this);
    } else {
      return options.inverse(this);
    }
}
exports.formatDate = function(datetime, format){
    if(moment){
        return moment(datetime).format(format);
    }else{
        return datetime;
    }
},
exports.status = function(status){
    var html ="";
    switch (status) {
        case ORDER_STATUS.New:
            html += "New";
            break;
        case ORDER_STATUS.Deployed:
            html += "Deployed";
            break;
        case ORDER_STATUS.Complete:
            html += "Complete";
            break;
        case ORDER_STATUS.Cancel:
            html += "Cancel";
            break;

    }
    return html;
},
    /**Format kiểu tiền tệ
     * @param {Number} c - Con số làm tròn mặc là 0
     * @param {String} d - Ký tự phân cách thập phân
     * @param {String} t - Ký tự phân cách hàng nghìn
     * @param {String} f - Ký tự đơn vị tiền tệ
     */
exports.formatMoney = function(value,c ,d ,t,f){
        c = isNaN((c = Math.abs(c))) ? 0 : c;
        d = d === undefined ? "," : d;
        t = t === undefined ? "." : t;
       var s = value < 0 ? "-" : "" ;
        f = f === undefined ? "₫" : f;
        var i = parseInt((value = Math.abs(+value || 0).toFixed(c))) + "";
       var  j = (j = i.length) > 3 ? j % 3 : 0;
    return (
        f +
        s +
        (j ? i.substr(0, j) + t : "") +
        i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
        (c
            ? d +
            Math.abs(value - i)
                .toFixed(c)
                .slice(2)
            : "")
    );
};
exports.calculator = function(a,b, cal){
    switch (cal) {
        case "+":
            return parseInt(a)+parseInt(b);
        case "-":
            return parseInt(a)-parseInt(b);
        case "*":
            return parseInt(a)*parseInt(b);
        case "/":
            return parseInt(a)/parseInt(b);
    }
},
exports.payStatus = function(val){
    var html ="";
    switch (val) {
        case PAYMENT_STATUS.NotPaid:
            html += "Not Paid";
            break;
        case PAYMENT_STATUS.Paid:
            html += "Paid";
            break;
    }
    return html;
},
exports.payMethod = function(val){
    var html ="";
    switch (val) {
        case PAYMENT_METHOD.COD:
            html += "COD";
            break;
        case PAYMENT_METHOD.CreditCard:
            html += "Credit Card";
        break;
     }
    return html;
},
exports.menu = async function(id){
    var html ="";
    var MenuParent = await MenuConfig.find({id: id});
    if(!MenuParent){
        var MenuChild = await MenuConfig.find({ParentId:id});
         MenuChild.forEach(e => {
            var chilst  = MenuChild.filter(c =>{
                return c.ParentId === e.Id;
            }).forEach(d =>{
                if(chilst.length > 0){
                    html+= "<li data-id='' class='nav-parent'>";
                    html+= "<a href='javascript:;' class='visible-sm visible-md'>"+d.Text+"<i class='demo-icon ecs-down-open-mini'></i></a>";
                    html+= "<a href='javascript:' class='visible-lg'>"+d.Text+"<i class='demo-icon ecs-down-open-mini'></i></a>";
                    html+= "</li>"
                }else{
                   html+= "<li data-id='' class=''><a href="+(d.Url != null ? d.Url : d.UrlAuto)+">"+d.Text+"</a></li>"
                }
            });

        })
    }
    return html;
};
/**
 * using
 * {{#parseJSON '{"id": "firstname", "label": "First name"}'}}
         {{> input }}
   {{/parseJSON}}
 */
exports.parseJSON = function(data, options){
    return options.fn(JSON.parse(data))
}
