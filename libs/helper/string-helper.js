/** Chuyển thành dạng URL string*/
String.prototype.cleanUnicode = function() {
  var str = this.toLowerCase();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  str = str.replace(
    /!|@@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|\;|\||\{|\}|\~|\“|\”|\™|\–|\-/g,
    "-"
  );
  str = str.replace(/^\-+|\-+$/g, "");
  str = str.replace(/\\/g, "");
  str = str.replace(/-+-/g, "-");
  return str;
};

/**Kiểm tra 1 chuỗi có phải email hay không */
String.prototype.isEmail = function() {
  var str = this.toLowerCase();
  var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return pattern.test(str);
};

/**Tạo random string bất kì */
String.randomString = function() {
  return (
    Math.random()
      .toString(36)
      .substring(2, 15) +
    Math.random()
      .toString(36)
      .substring(2, 15)
  );
};

/**Format kiểu tiền tệ
 * @param {Number} c - Con số làm tròn mặc là 0
 * @param {String} d - Ký tự phân cách thập phân
 * @param {String} t - Ký tự phân cách hàng nghìn
 * @param {String} f - Ký tự đơn vị tiền tệ
 */
Number.prototype.formatMoney = function(c, d, t, f) {
  var n = this,
    c = isNaN((c = Math.abs(c))) ? 0 : c,
    d = d == undefined ? "," : d,
    t = t == undefined ? "." : t,
    s = n < 0 ? "-" : "",
    f = f == undefined ? "₫" : f,
    i = parseInt((n = Math.abs(+n || 0).toFixed(c))) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
  return (
    f +
    s +
    (j ? i.substr(0, j) + t : "") +
    i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
    (c
      ? d +
        Math.abs(n - i)
          .toFixed(c)
          .slice(2)
      : "")
  );
};

Number.prototype.toDuringTime = function() {
  var n = this;
  if (n == 0) return "";
  if (n < 60) return `${n} Mins`;
  // if (n < 120) return `${n / 60} Hour`;
  return `${Math.floor(n / 60)} Hour ${n % 60} Minutes`;
  // else return `${n / 60} Hours`;
};

Number.prototype.toServicesCount = function() {
  var n = this;
  if (n == 0) return "";
  if (n == 1) return `${n} Service`;
  else return `${n} Services`;
};
