//Class Lưu thông tin lịch làm việc dùng chung cho tất cả các model
class ListWorkModel {
  constructor() {
    this.Sort = null;
    this.Day = null;
    this.Open = null;
    this.Close = null;
  }
}
module.exports = ListWorkModel;
