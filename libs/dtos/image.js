//Class Lưu thông tin ảnh dùng chung cho tất cả các model
class ImageModel {
  constructor() {
    this.Src = null;
    this.Alt = null;
    this.Url = null;
    this.Sort = 0;
    this.IsChecked = false;
  }
}
module.exports = ImageModel;
