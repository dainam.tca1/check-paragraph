// core js
// $('.lazy').lazy({
//   effect: "fadeIn"
// });
$(document).ready(function () {
    // Pre Load
    // setTimeout(function() {
    //     loadAPI();
    // }, 3000);
    setTimeout(function() {
        $('.lazy-wiget').css({
            opacity: '1',
            visibility: 'visible'
        });
    }, 3500);
    $(".buy").click(function() {
        var id = $(this)
          .parent()
          .parent()
          .parent()
          .attr("data-product");
        $("#loading").show();
        $.ajax({
          url: "/shopping-cart/addToCart",
          type: "POST",
          data: { id: id },
          beforeSend: function() {
            $("#loading").show();
          },
          success: function(response) {
            $("#loading").hide();
            if (response.status == "success") {
                $("#info-modal").modal("toggle");
            } else {
              //  toastr["error"](response.message, "Error");
            }
          }
        });
      });
    // setTimeout(function() {
    //     $('body').addClass('loaded');
    // }, 3000);
    // End Pre Load
    // Go top
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#scrollup').fadeIn();
        } else {
            $('#scrollup').fadeOut();
        }
    });
    $('#scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
    // End go top
    // Nice sroll
    // $("html").niceScroll({
    //     cursorborder: "",
    //     cursorcolor: "#527F65",
    //     boxzoom: true,
    //     cursorwidth: "3px",
    //     mousescrollstep: "55"
    // });
    // End nice sroll
    // Smooth Scroll  
    // $('a[href*="#"]').not('[href="#"]').not('[href="#0"]').click(function(event) {
    //     // On-page links
    //     if (
    //         location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
    //         location.hostname == this.hostname
    //     ) {
    //         // Figure out element to scroll to
    //         var target = $(this.hash);
    //         target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    //         // Does a scroll target exist?
    //         if (target.length) {
    //             // Only prevent default if animation is actually gonna happen
    //             event.preventDefault();
    //             $('html, body').animate({
    //                 scrollTop: target.offset().top
    //             }, 200, function() {
    //                 // Callback after animation
    //                 // Must change focus!
    //                 var $target = $(target);
    //                 $target.focus();
    //                 if ($target.is(":focus")) { // Checking if the target was focused
    //                     return false;
    //                 } else {
    //                     $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
    //                     $target.focus(); // Set focus again
    //                 };
    //             });
    //         }
    //     }
    // });
    // ver 2
    // $('body').scrollspy({ target: ".myScrollspy", offset: 50 });

    // // Add smooth scrolling on all links inside the navbar
    // $(".smoothscroll li a").on('click', function(event) {

    //     // Make sure this.hash has a value before overriding default behavior
    //     if (this.hash !== "") {

    //         // Prevent default anchor click behavior
    //         event.preventDefault();

    //         // Store hash
    //         var hash = this.hash;

    //         // Using jQuery's animate() method to add smooth page scroll
    //         // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
    //         $('html, body').animate({
    //             scrollTop: $(hash).offset().top
    //         }, 1000, function() {
    //             $("#btn-menu").removeClass('open');
    //             $("#nav-mobile").removeClass("showmenu");
    //             $("#nav-mobile-container").removeClass("showmenu");
    //             $("body").removeClass("showmenu");
    //             // Add hash (#) to URL when done scrolling (default click behavior)
    //             // window.location.hash = hash;
    //         });

    //     } // End if

    // });
    // End Smooth Scroll
    // Light Box
    // var $lightbox = $('#lightbox');
    // $('[data-target="#lightbox"]').on('click', function(event) {
    //     var $img = $(this).find('img'),
    //         src = $img.attr('src'),
    //         alt = $img.attr('alt'),
    //         css = {
    //             'maxWidth': $(window).width() - 100,
    //             'maxHeight': $(window).height() - 100
    //         };

    //     $lightbox.find('.close').addClass('hidden');
    //     $lightbox.find('img').attr('src', src);
    //     $lightbox.find('img').attr('alt', alt);
    //     $lightbox.find('img').css(css);
    // });
    // $lightbox.on('shown.bs.modal', function(e) {
    //     var $img = $lightbox.find('img');

    //     $lightbox.find('.modal-dialog').css({
    //         'width': $img.width()
    //     });
    //     $lightbox.find('.close').removeClass('hidden');
    // });
    // End Light Box
    // Stick top
    var navdesktop = $('.header-body').offset();
    $(window).scroll(function () {
        if ($(window).scrollTop() > navdesktop.top) {
            $('.header-body').addClass('nav-fix')
        } else {
            $('.header-body').removeClass('nav-fix')
        }
    });
    // var navmobile = $('#nav-mobile .nav-header').offset();
    // $(window).scroll(function () {
    //     if ($(window).scrollTop() > navmobile.top) {
    //         $('#nav-mobile .nav-header').addClass('nav-fix')
    //     } else {
    //         $('#nav-mobile .nav-header').removeClass('nav-fix')
    //     }
    // });
    // End Stick top
    // Stick bottom
    // console.log($(document).scrollTop());
    // var link = $('#example_element1');
    // var position = link.position(); //cache the position
    // var bottom = $(window).height() - position.top - link.height();
    // $(window).scroll(function() {
    //     if ($(window).scrollTop() > bottom) {
    //         $('#example_element1').addClass('fixed-footer')
    //         $('#example_element1').fadeIn();
    //     }
    //     if ($(window).scrollTop() < bottom) {
    //         $('#example_element1').fadeOut();
    //     }
    //     if ($(window).scrollTop() > 7498) {
    //         $('#example_element1').removeClass('fixed-footer')
    //     }
    // });
    // End stick bottom
    // Load gif
    // var body = document.getElementsByTagName('load')[0];
    // var removeLoading = function() {
    //   // In a production application you would remove the loading class when your
    //   // application is initialized and ready to go.  Here we just artificially wait
    //   // 3 seconds before removing the class.
    //   setTimeout(function() {
    //     $(".loading").fadeOut("slow");
    //   }, 3000);
    // };
    // removeLoading();
    // End Load gif
    // Right Click Disabled
    // document.onmousedown = disableclick;
    // status = "Right Click Disabled";

    // function disableclick(event) {
    //     if (event.button == 2) {
    //         alert(status);
    //         return false;
    //     }
    // }
    // End Right Click Disabled
    // Input number
    // $('.btn-number').click(function(e) {
    //     e.preventDefault();
    //     fieldName = $(this).attr('data-field');
    //     type = $(this).attr('data-type');
    //     var input = $("input[name='" + fieldName + "']");
    //     var currentVal = parseInt(input.val());
    //     if (!isNaN(currentVal)) {
    //         if (type == 'minus') {

    //             if (currentVal > input.attr('min')) {
    //                 input.val(currentVal - 1).change();
    //             }
    //             if (parseInt(input.val()) == input.attr('min')) {
    //                 $(this).attr('disabled', true);
    //             }

    //         } else if (type == 'plus') {

    //             if (currentVal < input.attr('max')) {
    //                 input.val(currentVal + 1).change();
    //             }
    //             if (parseInt(input.val()) == input.attr('max')) {
    //                 $(this).attr('disabled', true);
    //             }

    //         }
    //     } else {
    //         input.val(0);
    //     }
    // });
    // $('.input-number').focusin(function() {
    //     $(this).data('oldValue', $(this).val());
    // });
    // $('.input-number').change(function() {
    //     minValue = parseInt($(this).attr('min'));
    //     maxValue = parseInt($(this).attr('max'));
    //     valueCurrent = parseInt($(this).val());

    //     name = $(this).attr('name');
    //     if (valueCurrent >= minValue) {
    //         $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
    //     } else {
    //         alert('Sorry, the minimum value was reached');
    //         $(this).val($(this).data('oldValue'));
    //     }
    //     if (valueCurrent <= maxValue) {
    //         $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
    //     } else {
    //         alert('Sorry, the maximum value was reached');
    //         $(this).val($(this).data('oldValue'));
    //     }
    // });
    // $(".input-number").keydown(function(e) {
    //     // Allow: backspace, delete, tab, escape, enter and .
    //     if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
    //         // Allow: Ctrl+A
    //         (e.keyCode == 65 && e.ctrlKey === true) ||
    //         // Allow: home, end, left, right
    //         (e.keyCode >= 35 && e.keyCode <= 39)) {
    //         // let it happen, don't do anything
    //         return;
    //     }
    //     // Ensure that it is a number and stop the keypress
    //     if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    //         e.preventDefault();
    //     }
    // });
    // End input number
    // Auto hide stick menu
    // var elSelector = '.header-footer',
    //     elClassHidden = 'nav-fix-hidden',
    //     throttleTimeout = 100,
    //     $element = $(elSelector);

    // if (!$element.length) return true;
    // var elementPosition = $('.header-footer').offset();
    // var $window = $(window),
    //     wHeight = 0,
    //     wScrollCurrent = 0,
    //     wScrollBefore = 0,
    //     wScrollDiff = 0,
    //     $document = $(document),
    //     dHeight = 0,

    //     throttle = function(delay, fn) {
    //         var last, deferTimer;
    //         return function() {
    //             var context = this,
    //                 args = arguments,
    //                 now = +new Date;
    //             if (last && now < last + delay) {
    //                 clearTimeout(deferTimer);
    //                 deferTimer = setTimeout(function() {
    //                     last = now;
    //                     fn.apply(context, args);
    //                 }, delay);
    //             } else {
    //                 last = now;
    //                 fn.apply(context, args);
    //             }
    //         };
    //     };

    // $window.on('scroll', throttle(throttleTimeout, function() {
    //     dHeight = $document.height();
    //     wHeight = $window.height();
    //     wScrollCurrent = $window.scrollTop();
    //     wScrollDiff = wScrollBefore - wScrollCurrent;
    //     if ($(window).scrollTop() > elementPosition.top) {
    //         if (wScrollCurrent <= 0) // scrolled to the very top; element sticks to the top
    //             $element.removeClass(elClassHidden);

    //         else if (wScrollDiff > 0 && $element.hasClass(elClassHidden)) // scrolled up; element slides in
    //             $element.removeClass(elClassHidden);

    //         else if (wScrollDiff < 0) // scrolled down
    //         {
    //             if (wScrollCurrent + wHeight >= dHeight && $element.hasClass(elClassHidden)) // scrolled to the very bottom; element slides in
    //                 $element.removeClass(elClassHidden);

    //             else // scrolled down; element slides out
    //                 $element.addClass(elClassHidden);
    //         }
    //     }


    //     wScrollBefore = wScrollCurrent;
    // }));
    // End auto hide stick menu
    // Auto hide stick footer 
    // Hide Header on on scroll down
    // console.log(document.body.scrollHeight)
    // var didScroll;
    // var lastScrollTop = 0;
    // var delta = 5;
    // var navbarHeight = $('#footer-fixed').outerHeight();

    // $(window).scroll(function(event) {
    //     didScroll = true;
    // });

    // setInterval(function() {
    //     if (didScroll) {
    //         hasScrolled();
    //         didScroll = false;
    //     }
    // }, 250);

    // function hasScrolled() {
    //     var st = $(this).scrollTop();

    //     // Make sure they scroll more than delta
    //     if (Math.abs(lastScrollTop - st) <= delta)
    //         return;

    //     // If they scrolled down and are past the navbar, add class .nav-up.
    //     // This is necessary so you never see what is "behind" the navbar.
    //     if (st > lastScrollTop && st > navbarHeight) {
    //         // Scroll Down
    //         $('#footer-fixed').removeClass('nav-down').addClass('nav-up');
    //     } else {
    //         // Scroll Up
    //         if (st + $(window).height() < $(document).height()) {
    //             $('#footer-fixed').removeClass('nav-up').addClass('nav-down');
    //         }
    //     }

    //     lastScrollTop = st;
    // }
    // End auto hide stick footer
    // Gird layout
    // $.fn.masonry = function(params) {
    //     var defaults = {
    //         columns: 3
    //     };
    //     var options = $.extend(defaults, params),
    //         container = this,
    //         items = container.find('.item'),
    //         colCount = 0,
    //         columns = $(Array(options.columns + 1).join('<div></div>')).addClass('masonryColumn').appendTo(container);
    //     for (var c = 0; c < items.length; c++) {
    //         items.eq(c).appendTo(columns.eq(colCount));
    //         colCount = (colCount + 1 > (options.columns - 1)) ? 0 : colCount + 1;
    //     }
    // }
    // End gird layout
    // Custom style select
    // $('.select_mystyle').each(function () {
    //     var $this = $(this),
    //         $id = $(this).attr("name"),
    //         numberOfOptions = $(this).children('option').length;

    //     $this.addClass('select-hidden');
    //     $this.wrap('<div class="select" id="select_mystyle_' + $id + '"></div>');
    //     $this.after('<div class="select-styled"></div>');


    //     if ($this.children('option.selected').text() != "") {
    //         var $styledSelect = $this.next('div.select-styled');
    //         $styledSelect.text($this.children('option.selected').text());
    //     }
    //     else {
    //         var $styledSelect = $this.next('div.select-styled');
    //         $styledSelect.text($this.children('option').eq(0).text());
    //     }


    //     var $list = $('<ul />', {
    //         'class': 'select-options'
    //     }).insertAfter($styledSelect);

    //     for (var i = 0; i < numberOfOptions; i++) {
    //         $('<li />', {
    //             text: $this.children('option').eq(i).text(),
    //             rel: $this.children('option').eq(i).val()
    //         }).appendTo($list);
    //     }

    //     var $listItems = $list.children('li');

    //     $styledSelect.click(function (e) {
    //         e.stopPropagation();
    //         $('div.select-styled.active').not(this).each(function () {
    //             $(this).removeClass('active').next('ul.select-options').hide();
    //         });
    //         $(this).toggleClass('active').next('ul.select-options').toggle();
    //         $("html, body").animate({
    //             scrollTop: $("#filter-container").offset().top - 100
    //         }, 600);
    //     });

    //     $listItems.click(function (e) {
    //         e.stopPropagation();
    //         $styledSelect.text($(this).text()).removeClass('active');
    //         $this.val($(this).attr('rel'));
    //         $(this).parent().find("li").removeClass("active");
    //         $(this).addClass("active");
    //         $list.hide();
    //         //  $("html, body").animate({
    //         //     scrollTop: $(".select_mystyle").offset().top
    //         // }, 600);
    //          //alert($this.val());
    //         // console.log($this.val());
    //     });

    //     $(document).click(function () {
    //         $styledSelect.removeClass('active');
    //         $list.hide();
    //     });

    // });
    // End custom style select
    // Timeline
    // $(window).scroll(function() {
    //     $('#timeline-container .text-container').each(function() {
    //         var scrollTop = $(window).scrollTop(),
    //             elementOffset = $(this).offset().top,
    //             distance = (elementOffset - scrollTop),
    //             windowHeight = $(window).height(),
    //             breakPoint = windowHeight * 0.9;

    //         if (distance > breakPoint) {
    //             $(this).css({
    //                 'opacity': 0
    //             });
    //             $(this).addClass("more-padding");
    //         }
    //         if (distance < breakPoint) {
    //             $(this).css({
    //                 'opacity': 0 + (scrollTop / 1000)
    //             });
    //             $(this).removeClass("more-padding");
    //         }
    //     });
    // });
    // End time line
});
// end core js
// global js
    var loading = false;
    $(document).ready(function () {
        $("#nav-mobile .nav-content .nav-child").on("hide.bs.collapse", function (e) {
            if ($(this).is(e.target)) {
                $(this).parent().find('> .nav-parent-action-collapse:first-child > span.icon').html('<i class="demo-icon ecs-down-open-big"></i>');
            }
        });
        $("#nav-mobile .nav-content .nav-child").on("show.bs.collapse", function (e) {
            if ($(this).is(e.target)) {
                $(this).parent().find('> .nav-parent-action-collapse:first-child > span.icon').html('<i class="demo-icon ecs-up-open-big"></i>');
            }
        });
        $("footer .collapse").on("hide.bs.collapse", function (e) {
            if ($(this).is(e.target)) {
                $(this).parent().find('> h2:first-child > span').html('<i class="demo-icon ecs-down-open-big"></i>');
            }
        });
        $("footer .collapse").on("show.bs.collapse", function (e) {
            if ($(this).is(e.target)) {
                $(this).parent().find('> h2:first-child > span').html('<i class="demo-icon ecs-up-open-big"></i>');
            }
        });
        $("#js-facebook-chat").click(function () {
            $(this).toggleClass("open");
        });
        var targetfacebook = document.getElementById('js-facebook-content');
        var targetfacebook1= document.getElementById('facebook-opacity');
        $(document).on("click",function(event) {
            if (event.target != targetfacebook && event.target != targetfacebook1) {
                $("#js-facebook-chat").removeClass('open');
            }
        });
        $("#show-cart-container .btn-close").click(function(event) {
            $("#show-cart-container").toggleClass('showmenu');
            $("body").toggleClass('showmenu');
        });
        var targetcart = document.getElementById('show-cart-container');
        $(document).on("click",function(event) {
            if (event.target == targetcart) {
                $("#show-cart-container").toggleClass('showmenu');
                $("body").toggleClass('showmenu');
            }
        });
    });
    // Menu mobile
        $("#menu-btn").click(function () {
            $(this).toggleClass('open');
            $("#nav-mobile .nav-header").toggleClass("showmenu");
            $("#nav-mobile .nav-body").toggleClass("showmenu");
            //$("main").toggleClass("showmenu");
            //$("footer").toggleClass("showmenu");
            $(".nav-parent-action").next().removeClass("showmenu");
            if($("body").hasClass('showmenu1') == true) {
                $("body").removeClass("showmenu1");
            }
            $("body").toggleClass("showmenu");

            $("#nav-mobile .search-btn-action").removeClass('open');
            $("#nav-mobile .nav-footer").removeClass("showmenu");
        });
        $("#nav-mobile .search-btn-action").click(function () {
            $(this).toggleClass('open');
            $("#nav-mobile .nav-footer").toggleClass("showmenu");
            if($("body").hasClass('showmenu') == true) {
                $("body").removeClass("showmenu");
            }
            $("body").toggleClass("showmenu1");
            $("#menu-btn").removeClass('open');
            $("#nav-mobile .nav-header").removeClass("showmenu");
            $("#nav-mobile .nav-body").removeClass("showmenu");
            $("#nav-mobile .nav-parent-action").removeClass('open');
            $("#nav-mobile .nav-parent-action").next().removeClass("showmenu");
        });
        $("#nav-mobile .nav-parent-action .icon").click(function () {
            $(this).parent().toggleClass('open');
            $(this).parent().next().toggleClass("showmenu");
        });
        $("#nav-mobile .back-btn").click(function () {
            $(this).toggleClass('open');
            $(this).parent().parent().parent().removeClass('showmenu');
        });

        $("#btn-cart").click(function(event) {
            $("#show-cart-container").toggleClass('showmenu');
            $("body").toggleClass('showmenu');
        });     
    // End Menu mobile
    // Menu left
        // var slideIndex = 1;
        // $(document).ready(function () {
            // showSlides(slideIndex);
            // setTimeout(function() {
            //     var maxHeight = -1;
            //     $('.myslides').each(function() {
            //         maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
            //     });
            //     $("#navleft-1").css('height', maxHeight);
            //     $(".myslides").css('height', maxHeight);
            // }, 1);
        // });
        // function plusSlides(n) {
        //     showSlides(slideIndex += n);
        // }
        // function showSlides(n) {
        //   var i;
        //   var slides = document.getElementsByClassName("myslides");
        //   var dots = document.getElementsByClassName("dot");
          
        //   if (n == 1) {
        //     $("#btnslider-back").hide();
        //   }
        //   else {
        //     $("#btnslider-back").show();
        //   }
        //   if (n == slides.length) {
        //     $("#btnslider-next").hide();
        //   }
        //   else {
        //     $("#btnslider-next").show();
        //   }
        //   if (n > slides.length) {slideIndex = 1}    
        //   if (n < 1) {slideIndex = slides.length}
        //   for (i = 0; i < slides.length; i++) {
        //       // slides[i].style.display = "none";
        //       slides[i].className = slides[i].className.replace(" show", "");
        //   }
        //   // for (i = 0; i < dots.length; i++) {
        //   //     dots[i].className = dots[i].className.replace(" active", "");
        //   // }
        //   // slides[slideIndex-1].style.display = "block";
        //   slides[slideIndex-1].className += " show";
        //   // dots[slideIndex-1].className += " active";
        // }
    // End Menu left
    // Đặt hàng
        // function AddToCartdt(productId, quantityClass) {
        //     addToCart(productId, $(quantityClass).val());
        // }
        // var loading = false;
        // function addToCart(productId, quantity) {
        //     $.post("/aj/order/addtocart", { productId: productId, quantity: quantity }, function (data) {
        //         if (data.Code !== 200)
        //             alert(data.Msg);
        //         else {
        //             $("#info-modal").modal("show");
        //             $(".cart-count-txt").text(data.Msg);
        //         }
        //     })
        // }
        function AddToCartdt(productId, quantityClass) {
            addToCart(productId, $(quantityClass).val());
        }
        function addToCart(productId, quantity) {
            $.post("/aj/order/addtocart",
                { productId: productId, quantity: quantity },
                function(data) {
                    if (!data.Ok)
                        alert(data.Msg);
                    else {
                        $("#info-modal").modal("show");
                        $(".cart-count-txt").text(data.Data.TotalCount);
                        $(".cart-money").text(data.Data.TotalAmount);
                    }
                });
        }
    // end đặt hàng
    // search
        function cleanUnicode(str) {
            str = str.toLowerCase();
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
            str = str.replace(/đ/g, "d");
            str = str.replace(/!|@@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|\;|\||\{|\}|\~|\“|\”|\™|\–|\-/g, "-");
            str = str.replace(/^\-+|\-+$/g, "");
            str = str.replace(/\\/g, "");
            str = str.replace(/-+-/g, "-");
            return str;
        }
        function makekeyword(obj) {
            var target = $(obj).parent().parent();
            var value = cleanUnicode($(obj).val());
            //var link = "/tim-kiem?keyword=" + value + "&key=" + key;
            $(obj).focusout(function () {
                $(target).find(".pre-search").fadeOut();
            });
            $(obj).focusin(function () {
                $(target).find(".pre-search").fadeIn();
            });
            if (loading) {
                return false;
            }
            $.ajax({
                url: "/aj/Search/PreSearch",
                type: "GET",
                data: { keyword: value },
                beforeSend: function () {
                    loading = true;
                },
                success: function (data) {
                    loading = false;
                    if (data.Data == "") {
                        //alert(data.Data);
                        //console.log(data.Data);
                        $(target).find(".pre-search").fadeOut();
                    } else {
                        $(target).find(".pre-search").fadeIn();

                        $(target).find(".pre-search").html(data.Data);
                        //$('.pre-search .lazy').lazy({
                        //    effect: "fadeIn"
                        //});
                        //$(target).find(".pre-search").find(".btn").attr("href", link);
                    }
                },
                error: function () {
                    loading = false;
                    //$("#dlding").hide();
                }
            })
        }
    // End search
    // Gửi form
        function submitcontact(obj) {
            var target = $(obj).attr("id");
            $(obj).find(".btn").prop('disabled', true);
            $(obj).find(".btn-loading").addClass("loading");
            if (loading === true)
                return false;
            $.ajax({
                url: $("#" + target).attr("action"),
                dataType: "json",
                method: "POST",
                data: $(obj).serialize(),
                success: function (response) {
                    loading = false;
                    // console.log(response);
                    if (response.Ok == false) {
                        $(obj).find(".btn").prop('disabled', false);
                        $(obj).find(".btn-loading").removeClass("loading");
                        alert(response.Msg);
                    }
                    else {
                        $(obj).find(".btn-loading").removeClass("loading");
                        $("#noti-modal").modal('toggle');
                        $("#noti-modal").on('hide.bs.modal', function () {
                            document.location.replace("/");
                        });
                    }
                },
                beforeSend: function () {
                    loading = true;
                },
                error: function () {
                    loading = false;
                }
            });
            return false;
        }

        // function submitcontact(idview) {
        //     var form = $(idview);
        //     $.ajax({
        //         url: form.attr("action"),
        //         type: 'POST',
        //         data: form.serialize(),
        //         beforeSend: function () {
        //             $(".loading").show()
        //         },
        //         success: function (result) {
        //             $(".loading").hide()
        //             if (result.Ok) {
        //                 alert("Gửi thông tin thành công, nhân viên sẽ gọi liên hệ bạn trong thời gian sớm nhất.");
        //                 location.reload();
        //             }
        //             else
        //                 alert(result.Msg);
        //         },
        //         error: function () {
        //             $(".loading").hide()
        //             alert("Hệ thống gặp sự cố kỹ thuật vui lòng thử lại sau");
        //         }
        //     });
        // }
    // End gửi form
// end global js