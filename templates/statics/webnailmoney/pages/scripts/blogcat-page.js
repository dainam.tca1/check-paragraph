$(document).ready(function() {})
function refreshfilter() {
    var form = $("#filterform");
    form.find("input[name='sort']").val($("#sortdropdown").val());
    //form.find("input[name='page']").val(1);
    getfilterresult();
}

function getfilterresult() {
    var form = $("#filterform");
    $(".pre-load").show();
    $.get("/aj/newscategory/filternews",
        form.serialize(),
        function (res) {
            $(".pre-load").fadeOut();
            $("#news-list-filter").html(res);
            updateState();
        }).fail(function () {
            $(".pre-load").fadeOut();
            alert("Hệ thống gặp sự cố kỹ thuật vui lòng thử lại.");
        });
}

function updateState() {
    var frmData = $("#filterform").serializeArray();
    var qr = '', url = window.location.href.split('?')[0], title = document.title;
    for (var i = 0; i < frmData.length; i++) {
        var d = frmData[i];
        if (d.value === undefined || d.value === '')
            continue;
        switch (d.name) {
            case 'sort':
                qr += '&sort=' + d.value;
                break;
        }
    }

    //var cc = window.uQuery('clearcache');
    //if (cc != undefined && cc !== '' && cc !== 'null')
    //    qr += '&clearcache=' + cc;
    qr = qr.replace(/(^&)|(&$)/g, '');
    if (qr === '')
        qr = url;
    else
        qr = url + '?' + qr;
    window.history.pushState({ state: qr, rand: Math.random() }, title, qr);
}
function loadmoredocument(catid, obj) {
    $.ajax({
        url: "/aj/NewsCategory/FilterNews",
        type: 'POST',
        data: { catid: catid, page: $(obj).parent().find(".currentpage").val() },
        beforeSend: function () {
            $(".loading").show()
        },
        success: function (result) {
            $(".loading").hide();
            $(obj).parent().find(".flex").append(result.htmlcode);
            $(obj).parent().find(".currentpage").val(++result.CurrentPage);
            if (result.CurrentPage >= result.TotalPageCount) {
                $(obj).remove();
            }            
        },
        error: function () {
            $(".loading").hide()
            alert("Hệ thống gặp sự cố kỹ thuật vui lòng thử lại sau");
        }
    });
}