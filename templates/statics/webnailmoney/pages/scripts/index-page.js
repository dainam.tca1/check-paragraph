$(document).ready(function() {
  $(".slider-1item").owlCarousel({
    loop: true,
    autoplay: false,
    navText: [
      "<i class='demo-icon ecs-left-1'></i>",
      "<i class='demo-icon ecs-right-1'></i>"
    ],
    margin: 0,
    autoplayHoverPause: true,
    items: 1,
    singleItem: true,
    lazyLoad: true,
    nav: true,
    autoHeight: false,
    smartSpeed: 1000,
    responsive: {
      0: {
        nav: false,
        dots: true
      },
      768: {
        dots: true,
        nav: true
      },
      1024: {
        dots: true,
        nav: true
      }
    }
  });
  $("#blog-slider").owlCarousel({
    loop: false,
    autoplay: false,
    navText: [
      "<i class='demo-icon ecs-left-open-big'></i>",
      "<i class='demo-icon ecs-right-open-big'></i>"
    ],
    margin: 0,
    autoplayHoverPause: false,
    lazyLoad: true,
    autoHeight: false,
    smartSpeed: 1000,
    responsive: {
      0: {
        nav: false,
        dots: true,
        items: 2,
        margin: 15
      },
      768: {
        items: 3,
        nav: false,
        dots: true,
        margin: 25
      },
      1024: {
        items: 3,
        nav: false,
        dots: true,
        margin: 15
      },
      1300: {
        items: 3,
        dots: false,
        nav: true,
        margin: 20
      }
    }
  });
  $("#productcat-slider").owlCarousel({
    loop: false,
    autoplay: false,
    navText: [
      "<i class='demo-icon ecs-left-open-big'></i>",
      "<i class='demo-icon ecs-right-open-big'></i>"
    ],
    margin: 0,
    autoplayHoverPause: false,
    lazyLoad: true,
    autoHeight: false,
    smartSpeed: 1000,
    responsive: {
      0: {
        nav: false,
        dots: true,
        items: 1,
        margin: 15
      },
      768: {
        items: 3,
        dots: true,
        nav: false,
        margin: 10
      },
      1024: {
        items: 4,
        dots: true,
        nav: false,
        margin: 15
      },
      1300: {
        items: 4,
        dots: false,
        nav: true,
        margin: 20
      }
    }
  });
  $("#partner-slider").owlCarousel({
    loop: true,
    autoplay: false,
    navText: [
      "<i class='demo-icon ecs-left-open-big'></i>",
      "<i class='demo-icon ecs-right-open-big'></i>"
    ],
    margin: 0,
    autoplayHoverPause: true,
    lazyLoad: true,
    autoHeight: false,
    smartSpeed: 1000,
    nav: false,
    dots: false,
    responsive: {
      0: {
        items: 2
      },
      768: {
        items: 5,
        dots: true
      },
      1024: {
        items: 7,
        dots: true
      }
    }
  });
  $("#testimonial-slider").owlCarousel({
    loop: true,
    autoplay: true,
    navText: [
      "<i class='demo-icon ecs-left-open-big'></i>",
      "<i class='demo-icon ecs-right-open-big'></i>"
    ],
    margin: 0,
    autoplayHoverPause: true,
    lazyLoad: true,
    autoHeight: false,
    smartSpeed: 1000,
    nav: true,
    dots: false,
    responsive: {
      0: {
        items: 1,
        nav: false,
        dots: false
      },
      768: {
        items: 1,
        nav: true
      },
      1024: {
        items: 1,
        nav: true,
        dots: true
      },
      1920: {
        items: 1,
        nav: true,
        dots: true
      }
    }
  });
  $("#feature-slider").owlCarousel({
    loop: true,
    autoplay: true,
    navText: [
      "<i class='demo-icon ecs-left-1'></i>",
      "<i class='demo-icon ecs-right-1'></i>"
    ],
    margin: 0,
    autoplayHoverPause: true,
    lazyLoad: true,
    autoHeight: false,
    smartSpeed: 1000,
    nav: false,
    dots: true,
    responsive: {
      0: {
        items: 1,
        nav: false,
        dots: true
      },
      768: {
        items: 1,
        nav: false
      },
      1024: {
        items: 1,
        nav: true
      },
      1920: {
        items: 1,
        nav: true,
        dots: true
      }
    }
  });
});
