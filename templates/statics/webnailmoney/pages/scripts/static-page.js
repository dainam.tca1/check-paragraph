﻿function submitcontact(obj) {
    var target = $(obj).attr("id");
    $(obj).find(".btn").prop('disabled', true);
    $(obj).find(".btn-loading").addClass("loading");
    if (loading === true)
        return false;
    $.ajax({
        url: $("#" + target).attr("action"),
        dataType: "json",
        method: "POST",
        data: $(obj).serialize(),
        success: function (response) {
            loading = false;
            if (response.Ok == false) {
                $(obj).find(".btn").prop('disabled', false);
                $(obj).find(".btn-loading").removeClass("loading");
                $("#" + target).find(".error-lst").css("display", "block");
                $("#" + target).find(".error-lst ul").html("<li>" + response.Msg + "</li>")
                //alert(response.Msg);
            }
            else {
                $("#" + target).find(".error-lst").css("display", "none");
                $(obj).find(".btn-loading").removeClass("loading");
                $("#noti-modal").modal('toggle');
                $("#noti-modal").on('hide.bs.modal', function () {
                    location.reload();
                });
            }
        },
        beforeSend: function () {
            loading = true;
        },
        error: function () {
            loading = false;
        }
    });
    return false;
}