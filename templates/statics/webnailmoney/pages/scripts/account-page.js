$(document).ready(function() {
  $(".provincedropdown").change(function() {
    $(".districtdropdown")
      .val("")
      .empty();
    $(".warddropdown")
      .val("")
      .empty();
    if (!$(".provincedropdown").val()) return 0;
    $.ajax({
      url: "/aj/order/GetDropdownAddress",
      type: "POST",
      data: { provinceid: $(this).val() },
      beforeSend: function() {
        $(".loading").show();
      },
      success: function(result) {
        $(".loading").hide();
        $(".districtdropdown").append(
          '<option value="">--Chọn tỉnh / thành phố--</option>'
        );
        for (var i = 0; i < result.Data.DistrictDropdownList.length; i++) {
          $(".districtdropdown").append(
            '<option value="' +
              result.Data.DistrictDropdownList[i].Id +
              '">' +
              result.Data.DistrictDropdownList[i].Name +
              "</option>"
          );
        }
        $(".warddropdown").append(
          '<option value="">--Chọn phường/xã--</option>'
        );
      }
    });
    return 0;
  });
  $(".districtdropdown").change(function() {
    $(".warddropdown")
      .val("")
      .empty();
    if (!$(".districtdropdown").val()) return 0;
    $.ajax({
      url: "/aj/order/GetDropdownAddress",
      type: "POST",
      data: { districtid: $(this).val() },
      beforeSend: function() {
        $(".loading").show();
      },
      success: function(result) {
        $(".loading").hide();
        $(".warddropdown").append(
          '<option value="">--Chọn phường/xã--</option>'
        );
        for (var i = 0; i < result.Data.WardDropdownList.length; i++) {
          $(".warddropdown").append(
            '<option value="' +
              result.Data.WardDropdownList[i].Id +
              '">' +
              result.Data.WardDropdownList[i].Name +
              "</option>"
          );
        }
      }
    });
    return 0;
  });
});

function toggleform(obj) {
  if ($(obj).is(":checked")) {
    $(".changepassform").show();
  } else {
    $(".changepassform").hide();
  }
}
function submitregister() {
  var form = $("#registerform");
  var accept = form.find("input[name='accept']").prop("checked");
  if (!accept) {
    alert("Please accept the terms of use");
    return -1;
  }
  $("#loading").show();
  $.ajax({
    url: "/member/signup",
    type: "POST",
    data: form.serialize(),
    success: function(response) {
      $("#loading").hide();
      if (response.status == "success") {
        alert(response.message, "Press OK to Login");
        window.location.href = "/member/login";
      } else {
        alert(response.message);
      }
    },
    error: function(er) {
      $("#loading").hide();
      alert("Error Opps! System has some error. Please try again!");
    }
  });
}

function submitlogin() {
  var form = $("#loginform");
  $.ajax({
    url: $(form).attr("action"),
    type: "POST",
    data: $(form).serialize(),
    beforeSend: function() {
      $(".loading").show();
    },
    success: function(result) {
      $(".loading").hide();
      if (result.data) {
        alert(result.message);
        window.location = result.data.callbackurl;
      } else {
        alert(result.message);
      }
    }
  });
}
function submitextenallogin() {
  var form = $("#extenalloginform");
  $.ajax({
    url: $(form).attr("action"),
    type: "POST",
    data: $(form).serialize(),
    beforeSend: function() {
      $(".loading").show();
    },
    success: function(result) {
      $(".loading").hide();
      if (result.Ok) {
        window.location = result.Msg;
      } else {
        alert(result.Msg);
      }
    }
  });
}
/*
// function submitregister() {
//   var form = $("#registerform");
//   $.ajax({
//     url: $(form).attr("action"),
//     type: "POST",
//     data: $(form).serialize(),
//     beforeSend: function() {
//       $(".loading").show();
//     },
//     success: function(result) {
//       $(".loading").hide();
//       if (result.Ok) {
//         window.location = result.Msg;
//       } else {
//         alert(result.Msg);
//       }
//     }
//   });
// }
*/
/*function submitforgot() {
  var form = $("#forgotform");
  $.ajax({
    url: $(form).attr("action"),
    type: "POST",
    data: $(form).serialize(),
    beforeSend: function() {
      $(".loading").show();
    },
    success: function(result) {
      $(".loading").hide();
      if (result.Ok) {
        window.location = result.Msg;
      } else {
        alert(result.Msg);
      }
    }
  });
}
*/
function submitforgotpasswordform() {
  var form = $("#forgotpasswordform");
  $("#loading").show();
  $.ajax({
    url: "/member/forgotpassword",
    type: "POST",
    data: form.serialize(),
    success: function(response) {
      $("#loading").hide();
      if (response.status == "success") {
        alert(response.message, function() {
          location = "/";
        });
      } else {
        alert(response.message);
      }
    },
    error: function(er) {
      $("#loading").hide();
      alert("Opps! System has some error. Please try again!");
    }
  });
}
function submitaccountinfo() {
  var form = $("#accountinfoform");
  $.ajax({
    url: "/member/profile",
    type: "POST",
    data: $(form).serialize(),
    beforeSend: function() {
      $(".loading").show();
    },
    success: function(result) {
      $(".loading").hide();
      if (result.status == "success") {
        alert(result.message);
        window.location.reload();
      } else {
        alert(result.message);
      }
    }
  });
}

function submitupdateaddress() {
  var form = $("#updateaddressform");
  $.ajax({
    url: $(form).attr("action"),
    type: "POST",
    data: $(form).serialize(),
    beforeSend: function() {
      $(".loading").show();
    },
    success: function(result) {
      $(".loading").hide();
      if (result.Ok) {
        alert(result.Msg);
        window.location.reload();
      } else {
        alert(result.Msg);
      }
    }
  });
}

function submitaddaddress() {
  var form = $("#addaddressform");
  $.ajax({
    url: $(form).attr("action"),
    type: "POST",
    data: $(form).serialize(),
    beforeSend: function() {
      $(".loading").show();
    },
    success: function(result) {
      $(".loading").hide();
      if (result.Ok) {
        alert(result.Msg);
        window.location.reload();
      } else {
        alert(result.Msg);
      }
    }
  });
}

function removeaddress(profileId) {
  if (confirm("Địa chỉ bị xóa sẽ không thể khôi phục, bạn có muốn xóa?")) {
    $.ajax({
      url: "/aj/account/RemoveAddress",
      type: "POST",
      data: { profileId: profileId },
      beforeSend: function() {
        $(".loading").show();
      },
      success: function(result) {
        $(".loading").hide();
        if (result.Ok) {
          alert(result.Msg);
          window.location.reload();
        } else {
          alert(result.Msg);
        }
      }
    });
  }
}
function submitticket() {
  var form = $("#sm-ticketform");
  $.ajax({
    url: "/member/add-ticket",
    type: "POST",
    data: $(form).serialize(),
    beforeSend: function() {
      $(".loading").show();
    },
    success: function(response) {
      $(".loading").hide();
      if ((response.status = "success")) {
        alert(response.message);
        window.location.href = "/member/myticket";
      } else {
        alert(response.message);
      }
    }
  });
}
