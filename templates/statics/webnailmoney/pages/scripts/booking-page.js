$(document).ready(function() {
    $("#nav-services .item-body").on("hide.bs.collapse", function (e) {
        if ($(this).is(e.target)) {
            $(this).parent().find('.icon').html('<i class="demo-icon ecs-down-open-big"></i>');
        }
    });
    $("#nav-services .item-body").on("show.bs.collapse", function (e) {
        if ($(this).is(e.target)) {
            $(this).parent().find('.icon').html('<i class="demo-icon ecs-up-open-big"></i>');
        }
    });
	$('#staff-slider').owlCarousel({
        loop: false,
        autoplay: false,
        nav: false,
        navText: ["<i class='demo-icon ecs-left-open-big'></i>", "<i class='demo-icon ecs-right-open-big'></i>"],
        margin: 10,
        lazyLoad:false,
        smartSpeed: 1000,
        responsive: {
            0: {
        		items: 2,
            },
            768: {
            	items: 5,
                dots: true,
            }
        }
    });
    $('#hours-slider').owlCarousel({
        loop: false,
        autoplay: false,
        nav: false,
        dots: true,
        navText: ["<i class='demo-icon ecs-left-open-big'></i>", "<i class='demo-icon ecs-right-open-big'></i>"],
        margin: 0,
        lazyLoad:false,
        smartSpeed: 1000,
        responsive: {
            0: {
                items: 3,
            },
            768: {
                items: 4,
            }
        }
    });
});
$(function() {
    $('#datetimepicker').datetimepicker({
        format: "DD/MM/YYYY",
        icons: {
            time: 'glyphicon glyphicon-time',
            date: 'glyphicon glyphicon-calendar',
            up: 'demo-icon ecs-up-open-big',
            down: 'demo-icon ecs-down-open-big',
            previous: 'demo-icon ecs-left-open-big',
            next: 'demo-icon ecs-right-open-big',
            today: 'glyphicon glyphicon-screenshot',
            clear: 'glyphicon glyphicon-trash',
            close: 'demo-icon ecs-cancel'
        }
    });
});