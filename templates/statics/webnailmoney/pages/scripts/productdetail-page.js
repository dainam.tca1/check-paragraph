$(document).ready(function() {
    var $zoom = $('.zoom').magnify();
    $('#related-slider').owlCarousel({
        loop: false,
        autoplay: false,
        navText: ["<i class='demo-icon ecs-left-open-big'></i>", "<i class='demo-icon ecs-right-open-big'></i>"],
        margin: 0,
        autoplayHoverPause: false,
        lazyLoad:true,
        autoHeight:false,
        smartSpeed: 1000,
        responsive: {
            0: {
                nav: false,
                dots: true,
                items: 1,
                margin:15
            },
            768: {
                items: 3,
                dots: true,
                nav: false,
                margin:10
            },
            1024: {
                items: 4,
                dots: true,
                nav: false,
                margin:15
            },
            1300: {
                items: 4,
                dots: false,
                nav: true,
                margin:20
            }
        }
    });
    $('.tab-container').each(function(index, el) {
        var check = $(this).height();
        if(check < 50) {
            $(this).parent().next().hide();
            $(this).parent().css('height', 'auto').height();
        }
    });
    /* slider and thumbnail */
        var sync1 = $("#slider-product-detail-1");
        var sync2 = $("#slider-product-detail-2");
        var slidesPerPage = 4; //globaly define number of elements per page
        var syncedSecondary = true;
        sync1.owlCarousel({
            items: 1,
            slideSpeed: 2000,
            nav: false,
            autoplay: false,
            dots: false,
            smartSpeed: 200,
            slideSpeed: 500,
            mouseDrag: false,
            autoHeight: true,
            lazyLoad:true,
            navText: ["<i class='demo-icon ecs-left-open-mini'></i>", "<i class='demo-icon ecs-right-open-mini'></i>"],
            afterMove: function() {
                setTimeout(function() {
                    // Update Magnify when slide changes
                    $zoom.destroy().magnify();
                }, 800); // This number should match paginationSpeed option
            },
            loop: true
        }).on('changed.owl.carousel', syncPosition);
        sync2
            .on('initialized.owl.carousel', function() {
                sync2.find(".owl-item").eq(0).addClass("current");
            })
            .owlCarousel({
                // items: slidesPerPage,
                dots: false,
                navText: ["<i class='demo-icon ecs-left-open-mini'></i>", "<i class='demo-icon ecs-right-open-mini'></i>"],
                autoplay: false,
                smartSpeed: 200,
                slideSpeed: 500,
                margin: 10,
                slideBy: 1,
                mouseDrag: false,
                lazyLoad:true,
                responsive: {
                    320: {
                        items:3
                    },
                    768: {
                        items:4,
                        nav: false,
                    },
                    1024: {
                        items:3,
                        nav: false,
                    },
                    1360: {
                        items:4,
                        nav: true,
                    }
                }
                // slideBy: slidesPerPage
            }).on('changed.owl.carousel', syncPosition2);
        function syncPosition(el) {
            //if you set loop to false, you have to restore this next line
            //var current = el.item.index;

            //if you disable loop you have to comment this block
            var count = el.item.count - 1;
            var current = Math.round(el.item.index - (el.item.count / 2) - .5);

            if (current < 0) {
                current = count;
            }
            if (current > count) {
                current = 0;
            }

            //end block

            sync2
                .find(".owl-item")
                .removeClass("current")
                .eq(current)
                .addClass("current");
            var onscreen = sync2.find('.owl-item.active').length - 1;
            var start = sync2.find('.owl-item.active').first().index();
            var end = sync2.find('.owl-item.active').last().index();

            if (current > end) {
                sync2.data('owl.carousel').to(current, 100, true);
            }
            if (current < start) {
                sync2.data('owl.carousel').to(current - onscreen, 100, true);
            }
        }

        function syncPosition2(el) {
            if (syncedSecondary) {
                var number = el.item.index;
                sync1.data('owl.carousel').to(number, 100, true);
            }
        }

        sync2.on("click", ".owl-item", function(e) {
            e.preventDefault();
            var number = $(this).index();
            sync1.data('owl.carousel').to(number, 300, true);
        });
    /* End slider and thumbnail */
});
function openmore(obj) {
    var location = $(obj).parent();
    var target = $(location).prev();
    var el = target;
    curHeight = el.height();
    autoHeight = el.css('height', 'auto').height();
    el.height(curHeight).animate({height: autoHeight}, 1000);
    location.fadeOut();
}
function addReplyComment(id,proid) {
    var form = $("#formreply"+id);
    $.ajax({
        url: $(form).attr("action"),
        type: 'POST',
        data: $(form).serialize(),
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (result) {
            $(".loading").hide();
            if (result.Ok) {
                alert("Cảm ơn bạn đã bình luận / đánh giá sản phẩm, chúng tôi sẽ phản hồi trong thời gian sớm nhất.");
                location.reload();
            } else {
                alert(result.Msg);
            }
        }
    });
}

function addComment() {
    var form = $("#addcommentform");
    $.ajax({
        url: $(form).attr("action"),
        type: 'POST',
        data: $(form).serialize(),
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (result) {
            $(".loading").hide();
            if (result.Ok) {
                alert("Cảm ơn bạn đã bình luận / đánh giá sản phẩm, chúng tôi sẽ phản hồi trong thời gian sớm nhất.");
                location.reload();
            } else {
                alert(result.Msg);
            }
        }
    });
}

function showreply(objid) {
    $(".formreply").hide();
    $(objid).show();
}

function loadmorecomment(proid,page,pagesize) {
    $.ajax({
        url: "/aj/Product/LoadmoreComment",
        type: 'GET',
        data: { productId: proid, page: page, pagesize: pagesize },
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (result) {
            $(".loading").hide();
            $("#comment .col-loadmore").append(result.viewsrc);
            if (result.CurrentPage >= result.TotalPageCount)
                $("#loadmorecmt").hide();
            else
                $("#loadmorecmt").html("<a href=\"javascript:\" class=\"btn btn-primary\" onclick=\"loadmorecomment(" + proid + "," + (result.CurrentPage + 1) + "," + result.PageSize + ")\">Xem thêm</a>");

        }
    });
}

function addRating() {
    var form = $("#addratingform");
    $.ajax({
        url: $(form).attr("action"),
        type: 'POST',
        data: $(form).serialize(),
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (result) {
            $(".loading").hide();
            if (result.Ok) {
                alert("Cảm ơn bạn đã bình luận / đánh giá sản phẩm, chúng tôi sẽ phản hồi trong thời gian sớm nhất.");
                location.reload();
            } else {
                alert(result.Msg);
            }
        }
    });
}

function loadmorerating(proid, page, pagesize) {
    $.ajax({
        url: "/aj/Product/LoadmoreRating",
        type: 'GET',
        data: { productId: proid, page: page, pagesize: pagesize },
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (result) {
            $(".loading").hide();
            $("#danhgia .col-loadmore").append(result.viewsrc);
            if (result.CurrentPage >= result.TotalPageCount)
                $("#loadmorerating").hide();
            else
                $("#loadmorerating").html("<a href=\"javascript:\" class=\"btn btn-primary\" onclick=\"loadmorerating(" + proid + "," + (result.CurrentPage + 1) + "," + result.PageSize+")\">Xem thêm</a>");

        }
    });
}
// chose product ver
function initslideevent() {
    var $zoom = $('.zoom').magnify();
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.slider-nav',
        dots: true,
        infinite: false,
        adaptiveHeight: true,
        swipe: false,
        lazyLoad: "progressive"
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        arrows: false,
        focusOnSelect: true,
        vertical: true,
        verticalSwiping: true,
        infinite: false,
        draggable: true,
        lazyLoad: "progressive",
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 2,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    $('.slider-for').on('afterChange', function (event, slick, currentSlide, nextSlide) {
        $zoom.destroy().magnify();
    });
}

function choosesizecolor(curid) {
    var attrlst = [];
    $(".attrlst").each(function (i, e) {
        attrlst.push($(e).find("input:checked").val());
    });
    $.ajax({
        url: "/aj/product/choosecolorandsize",
        type: 'POST',
        data: { attrlst: attrlst, curid: curid },
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (result) {
            $(".loading").hide();
            if (result.Ok) {
                $("#productdetail > .section-header").html(result.Msg);
                initslideevent();
            } else {
                alert(result.Msg);
            }
        }
    });
}
// chose product ver