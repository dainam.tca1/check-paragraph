$(document).ready(function() {
	$('#related-slider').owlCarousel({
        loop: false,
        autoplay: false,
        navText: ["<i class='demo-icon ecs-left-open-big'></i>", "<i class='demo-icon ecs-right-open-big'></i>"],
        margin: 0,
        autoplayHoverPause: false,
        lazyLoad:true,
        autoHeight:false,
        smartSpeed: 1000,
        responsive: {
            0: {
                nav: false,
                dots: true,
                items: 1,
                margin:15
            },
            768: {
                items: 2,
                nav: false,
                dots: true,
                margin:25
            },
            1024: {
                items: 3,
                nav: false,
                dots: true,
                margin:15
            },
            1300: {
                items: 3,
                dots: false,
                nav: true,
                margin:20
            }
        }
    });
});
function showreply(objid) {
    $(".formreply").hide();
    $(objid).show();
    $("html, body").animate({
        scrollTop: $(objid).offset().top
    }, 600);
}

function addComment() {
    var form = $("#addcommentform");
    $.ajax({
        url: $(form).attr("action"),
        type: 'POST',
        data: $(form).serialize(),
        beforeSend: function () {
            $(form).find(".btn-loading").addClass("loading");
        },
        success: function (result) {
            $(form).find(".btn-loading").removeClass("loading");
            if (result.Ok) {
                alert("Cảm ơn bạn đã bình luận / đánh giá sản phẩm, chúng tôi sẽ phản hồi trong thời gian sớm nhất.");
                location.reload();
            } else {
                alert(result.Msg);
            }
        }
    });
}

function addReplyComment(id) {
    var form = $("#formreply" + id);
    $.ajax({
        url: $(form).attr("action"),
        type: 'POST',
        data: $(form).serialize(),
        beforeSend: function () {
            $(form).find(".btn-loading").addClass("loading");
        },
        success: function (result) {
            $(form).find(".btn-loading").removeClass("loading");
            if (result.Ok) {
                alert("Cảm ơn bạn đã bình luận / đánh giá sản phẩm, chúng tôi sẽ phản hồi trong thời gian sớm nhất.");
                location.reload();
            } else {
                alert(result.Msg);
            }
        }
    });
}

function loadmorecomment(obj, newsid, pagesize) {
    var link = $("#cmtlst .item-comment:last-child");
    var top = link.offset().top;
    var bottom = top + link.outerHeight() - 100;
    var nextpage = $(obj).parent().find("#currentIndex").val();
    $.ajax({
        url: "/aj/News/LoadmoreComment",
        type: 'GET',
        data: { newsid: newsid, page: nextpage, pagesize: pagesize },
        beforeSend: function () {
            $(obj).addClass("loading");
        },
        success: function (result) {
            $(obj).removeClass("loading");
            $(obj).parent().find("#currentIndex").val(result.CurrentPage + 1);
            $("#cmtlst").append(result.viewsrc);
            $("html, body").animate({
                scrollTop: bottom
            }, 600);
            if (result.CurrentPage >= result.TotalPageCount)
                $("#loadmorecmt").hide();
            //else
            //    $("#loadmorecmt").html("<a href=\"javascript:\" class=\"btn btn-primary btn-loading\" onclick=\"loadmorecomment(" + newsid + "," + (result.CurrentPage + 1) + "," + result.PageSize + ")\">Xem thêm bình luận</a>");

        }
    });
}

// function openpopupdownload(docid, docname) {
//     var form = $(".popup-download form");
//     form.find("input[name='docid']").val(docid);
//     form.find("input[name='title']").val("Đăng ký download tài liệu " + docname);
//     $(".popup-download").show();
//     $(".popup-ovl").show();
// }

// function submitdownload(idview) {
//     var form = $(idview);

//     $.ajax({
//         url: form.attr("action"),
//         type: 'POST',
//         data: form.serialize(),
//         beforeSend: function () {
//             $(".loading").show();
           
//         },
//         success: function (result) {
//             $(".loading").hide()
//             if (result.Ok) {
//                 location = result.Msg;
//             }
//             else
//                 alert(result.Msg);
//         },
//         error: function (x) {
//             $(".loading").hide()
//             alert("Hệ thống gặp sự cố kỹ thuật vui lòng thử lại sau");
//         }
//     });
// }