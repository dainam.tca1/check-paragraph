const { watch, src, dest, lastRun, parallel } = require("gulp");
var browserSync = require("browser-sync").create();
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
// var uglifyCss = require("gulp-uglifycss");
var imagemin = require("gulp-imagemin");
const cleanCSS = require("gulp-clean-css");
const autoprefixer = require("gulp-autoprefixer");
var minify = require("gulp-minify");
const {
  PATH_TEMPLATE_GLOBAL,
  PATH_TEMPLATE,
  PATH_TEMPLATE_NAME,
  PATH_TEMPLATE_PAGE,
  PATH_TEMPLATE_RELEASE
} = require("./libs/constants/PATH");
// var minifyCss = require("gulp-minify-css");
//#region admin
// auto build css folder admin all file in folder
function watchstyle() {
  watch(
    [
      "statics/templates/admin/global/css/*.css",
      "statics/templates/admin/layouts/layout/css/*.css"
    ],
    style
  );
}
// build css
function style() {
  return (
    src(
      [
        "statics/templates/admin/global/css/*.css",
        "statics/templates/admin/layouts/layout/css/*.css"
      ],
      { sourcemaps: true }
    )
      .pipe(concat("main.min.css"))
      .pipe(cleanCSS({ compatibility: "ie8" }))
      .pipe(
        autoprefixer({
          browsers: ["last 2 versions"],
          cascade: false
        })
      )
      // .pipe(
      //   uglifyCss({
      //     maxLineLen: 1,
      //     uglyComments: true
      //   })
      // )
      .pipe(dest("statics/templates/admin/release/css"))
  );
}
// auto build script folder admin (*)  all file in folder
function watchscript() {
  watch(
    [
      "statics/templates/admin/global/scripts/*.js",
      "statics/templates/admin/layouts/layout/scripts/*.js"
    ],
    script
  );
}
// build script
function script() {
  return src(
    [
      "statics/templates/admin/global/scripts/*.js",
      "statics/templates/admin/layouts/layout/scripts/*.js"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("main.js"))
    .pipe(uglify())

    .pipe(dest("statics/templates/admin/release/js"));
}
// auto build images folder admin (*) all file jpg
function watchimages() {
  watch("statics/templates/admin/layouts/layout/img/*", images);
}
// build images
function images() {
  return src("statics/templates/admin/layouts/layout/img/*", {
    since: lastRun(images)
  })
    .pipe(
      imagemin({ progressive: true, interlaced: true, optimizationLevel: 5 })
    )
    .pipe(dest("statics/templates/admin/release/images"));
}
//#endregion
//#region template
function watchmaincss() {
  watch(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "css/main-style.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "css/main-style-m.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/index-page.css",
      // PATH_TEMPLATE +
      //   PATH_TEMPLATE_NAME +
      //   PATH_TEMPLATE_PAGE +
      //   "css/blog-page.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/gallery-page.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/static-page.css"
    ],
    maincss
  );
}
function watchindexcss() {
  watch(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "css/main.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/index-page.css"
    ],
    indexcss
  );
}
function watchmainscript() {
  watch(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/jquery-3.2.1.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/jquery.cookie.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/bootstrap/js/bootstrap.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "scripts/script.js"
    ],
    mainscript
  );
}
function watchindexscript() {
  watch(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "scripts/static-page.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "script/main.min.js"
    ],
    indexscript
  );
}
// build css

function maincss() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/bootstrap/css/bootstrap.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "css/main-style.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "css/main-style-m.css"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("main.min.css"))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(
      dest(PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "css/")
    );
}
function mainscript() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/jquery.cookie.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/bootstrap/js/bootstrap.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "scripts/script.js"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("main.min.js"))
    .pipe(uglify())

    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    );
}

function bookingcss() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "css/main.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/owl-carousel/owl.carousel.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/owl-carousel/owl.theme.default.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/datetimepicker/bootstrap-datetimepicker.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/booking-page.css"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("booking-page.min.css"))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(
      dest(PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "css/")
    );
}
function bookingscript() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/jquery-3.2.1.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "script/main.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/owl-carousel/owl.carousel.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/datetimepicker/moment.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/datetimepicker/bootstrap-datetimepicker.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "scripts/booking-page.js"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("booking-page.min.js"))
    .pipe(uglify())

    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    );
}

function indexcss() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "css/main.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/owl-carousel/owl.carousel.min.css",
      // PATH_TEMPLATE +
      //   PATH_TEMPLATE_NAME +
      //   PATH_TEMPLATE_GLOBAL +
      //   "plugins/ilightbox/ilightbox.css",
      // PATH_TEMPLATE +
      //   PATH_TEMPLATE_NAME +
      //   PATH_TEMPLATE_GLOBAL +
      //   "plugins/ilightbox/metro-black-skin/skin.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/owl-carousel/owl.theme.default.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/index-page.css"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("index-page.min.css"))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(
      dest(PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "css/")
    );
}
function indexscript() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "script/main.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/owl-carousel/owl.carousel.min.js",
      // PATH_TEMPLATE +
      //   PATH_TEMPLATE_NAME +
      //   PATH_TEMPLATE_GLOBAL +
      //   "plugins/ilightbox/ilightbox.packed.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/mixitup/jquery.mixitup.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "scripts/index-page.js"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("index-page.min.js"))
    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    )
    .pipe(uglify())
    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    );
}
function cartcss() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "css/main.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/cart-page.css"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("cart-page.min.css"))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(
      dest(PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "css/")
    );
}
function cartscript() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "script/main.min.js",
      "statics/js/shoppingcart.js"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("cart-page.min.js"))
    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    )
    .pipe(uglify())
    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    );
}
function watchcartcss() {
  watch(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "css/main.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/cart-page.css"
    ],
    cartcss
  );
}
function watchcartscript() {
  watch(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "script/main.min.js",
      "statics/js/shoppingcart.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "cart-page.min.js"
    ],
    cartscript
  );
}
// function servicecatcss() {
//   return src(
//     [
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_RELEASE +
//         "css/main.min.css",
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_PAGE +
//         "css/service-page.css"
//     ],
//     { sourcemaps: true }
//   )
//     .pipe(concat("servicecat-page.min.css"))
//     .pipe(cleanCSS({ compatibility: "ie8" }))
//     .pipe(
//       autoprefixer({
//         browsers: ["last 2 versions"],
//         cascade: false
//       })
//     )
//     .pipe(
//       dest(PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "css/")
//     );
// }
// function servicecatscript() {
//   return src(
//     [
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_RELEASE +
//         "script/main.min.js",
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_PAGE +
//         "scripts/servicecat-page.js"
//     ],
//     { sourcemaps: true }
//   )
//     .pipe(concat("servicecat-page.min.js"))
//     .pipe(
//       dest(
//         PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
//       )
//     )
//     .pipe(uglify())
//     .pipe(
//       dest(
//         PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
//       )
//     );
// }

// function servicedetailcss() {
//   return src(
//     [
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_RELEASE +
//         "css/main.min.css",
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_PAGE +
//         "css/service-page.css"
//     ],
//     { sourcemaps: true }
//   )
//     .pipe(concat("servicedetail-page.min.css"))
//     .pipe(cleanCSS({ compatibility: "ie8" }))
//     .pipe(
//       autoprefixer({
//         browsers: ["last 2 versions"],
//         cascade: false
//       })
//     )
//     .pipe(
//       dest(PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "css/")
//     );
// }
// function servicedetailscript() {
//   return src(
//     [
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_RELEASE +
//         "script/main.min.js",
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_PAGE +
//         "scripts/servicedetail-page.js"
//     ],
//     { sourcemaps: true }
//   )
//     .pipe(concat("servicedetail-page.min.js"))
//     .pipe(
//       dest(
//         PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
//       )
//     )
//     .pipe(uglify())
//     .pipe(
//       dest(
//         PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
//       )
//     );
// }

function gallerycatcss() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "css/main.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/gallery-page.css"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("gallerycat-page.min.css"))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(
      dest(PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "css/")
    );
}
function gallerycatscript() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "script/main.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/mixitup/jquery.mixitup.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "scripts/gallerycat-page.js"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("gallerycat-page.min.js"))

    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    )
    .pipe(uglify())
    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    );
}

function gallerydetailcss() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "css/main.min.css",
      // PATH_TEMPLATE +
      //   PATH_TEMPLATE_NAME +
      //   PATH_TEMPLATE_GLOBAL +
      //   "plugins/fancybox/jquery.fancybox.min.css",
      // PATH_TEMPLATE +
      //   PATH_TEMPLATE_NAME +
      //   PATH_TEMPLATE_GLOBAL +
      //   "plugins/ilightbox/ilightbox.css",
      // PATH_TEMPLATE +
      //   PATH_TEMPLATE_NAME +
      //   PATH_TEMPLATE_GLOBAL +
      //   "plugins/ilightbox/metro-black-skin/skin.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/gallery-page.css"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("gallerydetail-page.min.css"))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(
      dest(PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "css/")
    );
}
function gallerydetailscript() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "script/main.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/lazy-load/jquery.lazy.min.js",
      // PATH_TEMPLATE +
      //   PATH_TEMPLATE_NAME +
      //   PATH_TEMPLATE_GLOBAL +
      //   "plugins/ilightbox/jquery.requestAnimationFrame.js",
      // PATH_TEMPLATE +
      //   PATH_TEMPLATE_NAME +
      //   PATH_TEMPLATE_GLOBAL +
      //   "plugins/ilightbox/jquery.mousewheel.js",
      // PATH_TEMPLATE +
      //   PATH_TEMPLATE_NAME +
      //   PATH_TEMPLATE_GLOBAL +
      //   "plugins/ilightbox/ilightbox.packed.js",
      // PATH_TEMPLATE +
      //   PATH_TEMPLATE_NAME +
      //   PATH_TEMPLATE_GLOBAL +
      //   "plugins/fancybox/jquery.fancybox.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "scripts/gallerydetail-page.js"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("gallerydetail-page.min.js"))
    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    )
    .pipe(uglify())
    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    );
}

// function blogcatcss() {
//   return src(
//     [
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_RELEASE +
//         "css/main.min.css",
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_PAGE +
//         "css/blog-page.css"
//     ],
//     { sourcemaps: true }
//   )
//     .pipe(concat("blogcat-page.min.css"))
//     .pipe(cleanCSS({ compatibility: "ie8" }))
//     .pipe(
//       autoprefixer({
//         browsers: ["last 2 versions"],
//         cascade: false
//       })
//     )
//     .pipe(
//       dest(PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "css/")
//     );
// }
// function blogcatscript() {
//   return src(
//     [
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_RELEASE +
//         "script/main.min.js",
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_PAGE +
//         "scripts/blogcat-page.js"
//     ],
//     { sourcemaps: true }
//   )
//     .pipe(concat("blogcat-page.min.js"))

//     .pipe(
//       dest(
//         PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
//       )
//     )
//     .pipe(uglify())
//     .pipe(
//       dest(
//         PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
//       )
//     );
// }

// function blogdetailcss() {
//   return src(
//     [
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_RELEASE +
//         "css/main.min.css",
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_GLOBAL +
//         "plugins/owl-carousel/owl.carousel.min.css",
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_GLOBAL +
//         "plugins/owl-carousel/owl.theme.default.min.css",
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_PAGE +
//         "css/blog-page.css"
//     ],
//     { sourcemaps: true }
//   )
//     .pipe(concat("blogdetail-page.min.css"))
//     .pipe(cleanCSS({ compatibility: "ie8" }))
//     .pipe(
//       autoprefixer({
//         browsers: ["last 2 versions"],
//         cascade: false
//       })
//     )
//     .pipe(
//       dest(PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "css/")
//     );
// }
// function blogdetailscript() {
//   return src(
//     [
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_RELEASE +
//         "script/main.min.js",
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_GLOBAL +
//         "plugins/owl-carousel/owl.carousel.min.js",
//       PATH_TEMPLATE +
//         PATH_TEMPLATE_NAME +
//         PATH_TEMPLATE_PAGE +
//         "scripts/blogdetail-page.js"
//     ],
//     { sourcemaps: true }
//   )
//     .pipe(concat("blogdetail-page.min.js"))

//     .pipe(
//       dest(
//         PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
//       )
//     )
//     .pipe(uglify())
//     .pipe(
//       dest(
//         PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
//       )
//     );
// }

function staticcss() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "css/main.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/static-page.css"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("static-page.min.css"))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(
      dest(PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "css/")
    );
}
function staticscript() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "script/main.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "scripts/static-page.js"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("static-page.min.js"))

    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    )
    .pipe(uglify())
    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    );
}
function accountcss() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "css/main.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/account-page.css"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("account-page.min.css"))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(
      dest(PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "css/")
    );
}
function accountscript() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "script/main.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "scripts/account-page.js"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("account-page.min.js"))

    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    )
    .pipe(uglify())
    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    );
}
function templatescatcss() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "css/main.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/product-page.css"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("productcat-page.min.css"))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(
      dest(PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "css/")
    );
}
function templatescatscript() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "script/main.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "scripts/productcat-page.js"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("productcat-page.min.js"))

    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    )
    .pipe(uglify())
    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    );
}
function productcatcss() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "css/main.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/product-page.css"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("productcat-page.min.css"))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(
      dest(PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "css/")
    );
}
function productcatscript() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "script/main.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "scripts/productcat-page.js"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("productcat-page.min.js"))
    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    )
    .pipe(uglify())
    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    );
}
function productdetailcss() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "css/main.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/owl-carousel/owl.carousel.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/owl-carousel/owl.theme.default.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/slick/slick.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/magnify-master/magnify.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/product-page.css"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("productdetail-page.min.css"))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(
      dest(PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "css/")
    );
}
function productdetailscript() {
  return src(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "script/main.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/magnify-master/jquery.magnify.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/slick/slick.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_GLOBAL +
        "plugins/owl-carousel/owl.carousel.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "scripts/productdetail-page.js"
    ],
    { sourcemaps: true }
  )
    .pipe(concat("productdetail-page.min.js"))
    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    )
    .pipe(uglify())
    .pipe(
      dest(
        PATH_TEMPLATE + PATH_TEMPLATE_NAME + PATH_TEMPLATE_RELEASE + "script/"
      )
    );
}
function watchproductcatcss() {
  watch(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "css/main.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/product-page.css"
    ],
    productcatcss
  );
}
function watchproductdetailcss() {
  watch(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "css/main.min.css",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "css/product-page.css"
    ],
    productdetailcss
  );
}
function watchproductcatscript() {
  watch(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "script/main.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "scripts/productcat-page.js"
    ],
    productcatscript
  );
}
function watchproductdetailscript() {
  watch(
    [
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_RELEASE +
        "script/main.min.js",
      PATH_TEMPLATE +
        PATH_TEMPLATE_NAME +
        PATH_TEMPLATE_PAGE +
        "scripts/productdetail-page.js"
    ],
    productdetailscript
  );
}
//#endregion
// build tung module
exports.watchstyle = watchstyle;
exports.watchscript = watchscript;
exports.style = style;
exports.script = script;
exports.images = images;

exports.watchmaincss = watchmaincss;
exports.watchindexcss = watchindexcss;
exports.watchmainscript = watchmainscript;
exports.watchindexscript = watchindexscript;
exports.watchcartscript = watchcartscript;
exports.watchcartcss = watchcartcss;
exports.productcatcss = productcatcss;
exports.productdetailcss = productdetailcss;
exports.productcatscript = productcatscript;
exports.productdetailscript = productdetailscript;
exports.watchproductcatcss = watchproductcatcss;
exports.watchproductcatscript = watchproductcatscript;
exports.watchproductdetailscript = watchproductdetailscript;
exports.watchproductdetailcss = watchproductdetailcss;
exports.maincss = maincss;
exports.mainscript = mainscript;

exports.bookingcss = bookingcss;
exports.bookingscript = bookingscript;

exports.indexcss = indexcss;
exports.indexscript = indexscript;

// exports.servicecatcss = servicecatcss;
// exports.servicecatscript = servicecatscript;

// exports.servicedetailcss = servicedetailcss;
// exports.servicedetailscript = servicedetailscript;

exports.gallerycatcss = gallerycatcss;
exports.gallerycatscript = gallerycatscript;

exports.gallerydetailcss = gallerydetailcss;
exports.gallerydetailscript = gallerydetailscript;

// exports.blogcatcss = blogcatcss;
// exports.blogcatscript = blogcatscript;

exports.cartcss = cartcss;
exports.cartscript = cartscript;
// exports.blogdetailcss = blogdetailcss;
// exports.blogdetailscript = blogdetailscript;

exports.staticcss = staticcss;
exports.staticscript = staticscript;

exports.templatescatcss = templatescatcss;
exports.templatescatscript = templatescatscript;

exports.accountcss = accountcss;
exports.accountscript = accountscript;
// build tat ca module

exports.main = parallel(maincss, mainscript);
exports.build = parallel(
  // maincss,
  // mainscript,
  // bookingcss,
  // bookingscript,
  indexcss,
  indexscript,
  cartcss,
  cartscript,
  // servicecatcss,
  // servicecatscript,
  // servicedetailcss,
  // servicedetailscript,
  // gallerycatcss,
  // gallerycatscript,
  // gallerydetailcss,
  // gallerydetailscript,
  // blogcatcss,
  // blogcatscript,
  // blogdetailcss,
  // blogdetailscript,

  staticcss,
  staticscript,
  accountcss,
  accountscript,
  templatescatcss,
  templatescatscript
);
exports.buildindex = parallel(watchmaincss, watchindexcss);
