//Xử lý menu
$(".page-sidebar-menu .nav-item[data-id='khuyen-mai']").addClass("active").addClass("open");
$(".page-sidebar-menu .nav-item[data-id='khuyen-mai'] > a > .arrow").addClass("open");
$(".page-sidebar-menu .nav-item[data-id='coupon-giam-gia']").addClass("active").addClass("open");

function ajaxfilter() {
    if ($(".filter-is .active").val()) {
        $(".tag-item[data-id='" + $(".filter-is .active").attr("data-id") + "']").remove();
        var html = "<div class=\"tag-item\" onclick=\"$(this).remove();SearchItem();\" data-id=\"" + $(".filter-is .active").attr("data-id") + "\" data-value=\"" + $(".filter-is .active").val() + "\"><span>" + $(".filter-is .active").attr("data-name") + ": " + $(".filter-is .active").val() + " <i class=\"fa fa-times\"></i></span></div>";
        $(".tag-list").append(html);
        SearchItem();
        $(".filter-option").fadeToggle();
    }
}
function SearchItem() {
    $.ajax({
        url: '/admin/DiscountCoupon/Search',
        type: 'GET',
        data: {
            code: $(".tag-item[data-id='code']").attr("data-value"),
            pagesize: $('#pagesize').val()
        },
        success: function (result) {
            $('.Ajax-Table').html(result);
        },
    });
}

function EditList() {
    var model = [];
    $(".Ajax-Table input[name='Id']").each(function (index, value) {
        model.push({
            Id: parseInt($(value).val()),
            Sort: parseInt($(value).parent().parent().find("input[name='Sort']").val())
        });
    });
    $.post(
        "/admin/product/UpdateList",
        { model: model },
        function (result) {
            alert(result.Msg);
            window.location.reload();
        }
    );
}
