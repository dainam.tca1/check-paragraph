﻿//Xử lý menu
$(".page-sidebar-menu .nav-item[data-id='san-pham']").addClass("active").addClass("open");
$(".page-sidebar-menu .nav-item[data-id='san-pham'] > a > .arrow").addClass("open");
$(".page-sidebar-menu .nav-item[data-id='danh-muc-san-pham']").addClass("active").addClass("open");

var FormValidation = function () {

    // validation using icons
    var handleValidation2 = function () {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form2 = $('#myform');
        var error2 = $('.alert-danger', form2);
        var success2 = $('.alert-success', form2);

        form2.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                name: {
                    required: true,
                    //remote: {
                    //    url: form2.find("#name").attr("data-val-remote-url"),
                    //    data: {
                    //        current_name: form2.find("#current_name").val(),
                    //    }
                    //}
                },

            },
            messages: {
                name: {
                    required: "Bạn chưa điền tên danh mục",
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit 
                var htmlerror = "";
                $(validator.errorList).each(function (index, value) {
                    htmlerror += "<li>" + value.message + "</li>";
                });
                error2.find("ul").html(htmlerror);
                success2.hide();
                error2.show();
                App.scrollTo(error2, -200);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");
                icon.attr("data-original-title", error.text()).tooltip({ 'container': 'body' });
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight

            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },

            submitHandler: function (form) {
                //success2.show();
                error2.hide();
                for (instance in CKEDITOR.instances)
                    CKEDITOR.instances[instance].updateElement();
                //form[0].submit();
                $.post(form.action, $(form).serialize(), function (response) {
                    if (response.Code === 200) {
                        alert("Cập nhật dữ liệu thành công");
                        window.location.reload();
                    }
                    else {
                        var htmlerror = "<li>" + response.Msg + "</li>";
                        error2.find("ul").html(htmlerror);
                        success2.hide();
                        error2.show();
                    }
                });
            }
        });


    }


    return {
        //main function to initiate the module
        init: function () {
            handleValidation2();
        }

    };

}();

jQuery(document).ready(function () {
    FormValidation.init();
    CKEDITOR.replace('Description', {
        startupMode: "wysiwyg"
    });

    $(".sortable").sortable({
        placeholder: "ui-state-highlight",
        update: function (event, ui) {
            //sortableup(ui.item.parent().find(".ui-sortable-handle"));
            var sortableClass = ui.item.parent();
            updateimgsort(getimglst(sortableClass), sortableClass);
        }
    });

    $("select[name='ParentId']").change(function () {
        $("input[name='CategoryLevel']").val((parseInt($(this).find(":selected").attr("data-level")) + 1));
    });
});




function addLinks(value) {
    $("input[name='Url']").val(cleanUnicode(value));
    $("input[name='MetaTitle']").val(value);
}


function upanhs(obj) {
    var data = new FormData();
    var files = $(obj).get(0).files;
    if (files.length > 0) {
        for (var i = 0; i < files.length; i++) {
            if (files[i].size > (1024 * 1024 * 2)) {
                alert("File ảnh không được quá 1MB");
                return -1;
            }
            else {
                data.append("HelpSectionImages" + i, files[i]);
            }
        }
    }

    $(obj).val("");
    Pace.track(function () {
        $.ajax({
            url: "/admin/AjaxHelper/UploadImg?pathStr=/img/product-category/",
            type: "POST",
            processData: false,
            contentType: false,
            data: data,
            success: function (response) {
                if (response.Code !== 200) {
                    alert(response.Msg);
                    return false;
                }
                var sortTableClass = $(obj).parent().parent().parent().find(".sortable");
                var imglst = getimglst(sortTableClass);
                var lstfile = JSON.parse(response.Msg);
                $(lstfile).each(function (index, value) {
                    imglst.push({
                        Id: null,
                        Src: value,
                        Alt: null
                    });
                });
                updateimgsort(imglst, sortTableClass);
                alert("Upload ảnh thành công");
                return false;
            },
            error: function (er) {
                alert("Lỗi kết nối");
            }
        });
    });
    return 1;
}

function getimglst(sortableClass) {
    var imglst = [];
    $(sortableClass).find("tr").each(function (index, value) {
        imglst.push({
            Id: !$(value).find(".item-id").val() ? null : parseInt($(value).find(".item-id").val()),
            Src: $(value).find(".item-src").val(),
            Alt: $(value).find(".item-alt").val()
        });
    });
    return imglst;
}

function updateimgsort(imgLst, sortableClass) {
    $.post("/admin/productcategory/updatesort",
        { ImgList: imgLst, ListName: $(sortableClass).data("name") },
        function (response) {
            $(sortableClass).html(response);
        });
}

function deleteimg(obj) {
    var cfirm = confirm("Bạn có chắc muốn xóa hình ảnh?");
    if (cfirm === true) {
        var sortableClass = $(obj).parent().parent().parent();
        $(obj).parent().parent().remove();
        updateimgsort(getimglst($(sortableClass)), $(sortableClass));
    }

}

function upanh(obj) {
    var result = { Code: 200, Msg: "" };
    var data = new FormData();
    var files = $(obj).get(0).files;
    if (files.length > 0) {
        for (var i = 0; i < files.length; i++) {
            if (files[i].size > (1024 * 1024)) {
                alert("File ảnh không được quá 1MB");
                return -1;
            }
            else {
                data.append("HelpSectionImages" + i, files[i]);
            }
        }
    }
    //$(obj).parent().find("img").attr("src", "/Templates/Admin/global/img/loading-spinner-grey.gif");
    $(obj).val("");
    Pace.track(function () {
        $.ajax({
            url: "/admin/AjaxHelper/UploadImg?pathStr=/img/product-category/",
            type: "POST",
            processData: false,
            contentType: false,
            data: data,
            success: function (response) {
                var lstfile = JSON.parse(response.Msg);
                $(obj).parent().find("img").attr("src", lstfile[0]);
                $(obj).parent().find(".item-avatar").val(lstfile[0]);
                alert("Upload ảnh thành công");
            },
            error: function (er) {
                alert("Lỗi kết nối");
            }
        });
    });

}