//Xử lý menu
$(".page-sidebar-menu .nav-item[data-id='tin-tuc']").addClass("active").addClass("open");
$(".page-sidebar-menu .nav-item[data-id='tin-tuc'] > a > .arrow").addClass("open");
$(".page-sidebar-menu .nav-item[data-id='danh-sach-tin-tuc']").addClass("active").addClass("open");

function ajaxfilter() {
    if ($(".filter-is .active").val()) {
        $(".tag-item[data-id='" + $(".filter-is .active").attr("data-id") + "']").remove();
        var html = "<div class=\"tag-item\" onclick=\"$(this).remove();SearchItem();\" data-id=\"" + $(".filter-is .active").attr("data-id") + "\" data-value=\"" + $(".filter-is .active").val() + "\"><span>" + $(".filter-is .active").attr("data-name") + ": " + $(".filter-is .active").val() + " <i class=\"fa fa-times\"></i></span></div>";
        $(".tag-list").append(html);
        SearchItem();
        $(".filter-option").fadeToggle();
    }
}
function SearchItem() {
    $.ajax({
        url: '/admin/news/Search',
        type: 'GET',
        data: {
            title: $(".tag-item[data-id='title']").attr("data-value"),
            CategoryId: $(".tag-item[data-id='news_category_id']").attr("data-value"),
            pagesize: $('#pagesize').val()
        },
        success: function (result) {
            $('.Ajax-Table').html(result);
        },
    });
}

function EditList() {
    var model = [];
    $(".Ajax-Table input[name='Id']").each(function (index, value) {
        model.push({
            Id: parseInt($(value).val()),
            Sort: parseInt($(value).parent().parent().find("input[name='Sort']").val())
        });
    });
    $.post(
        "/admin/news/UpdateList",
        { model: model },
        function (result) {
            alert(result.Msg);
            window.location.reload();
        }
    );
}
