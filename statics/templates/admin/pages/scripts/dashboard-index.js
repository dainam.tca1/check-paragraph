﻿$(".page-sidebar-menu .nav-item[data-id='tong-quan']").addClass("active").addClass("open");
$(document).ready(function () {

    $(function () {
        $('.portlet-body .scroller').slimscroll({
            height: '300px',
            alwaysVisible: true,
            wheelStep: 10,
            opacity: .4,
            color: '#bbbbbb',
        });
    });

    $("#btn-loadmore").click(function () {
        var last_id = $(".last_id:last-child").val();

        $.ajax({
            url: "/dashboard/readmore",
            type: "POST",
            data: { last_id: last_id },
            success: function (html) {
                $("#feeds_ajax").append(html);
            },
            error: function (er) {
                alert("Lỗi kết nối");
            }
        });
    });

    $.get("/Admin/dashboard/today", {
    }, function (data_ajax) {
        for (i = 0; i < data_ajax.spbanchaydata.length; i++) {
            var j = i + 1;
            $("#listsptab1").append("<div class='sp-banchay-item'><div class='dashboard-spbanchay-left'><img src='" + data_ajax.spbanchaydata[i].image + "'/><span> " + j + " </span></div><div class='dashboard-spbanchay-right'>" + data_ajax.spbanchaydata[i].Name + "</a><br />(Đã bán " + data_ajax.spbanchaydata[i].quantity + " sản phẩm)</div>")
        }
        var data = {
            labels: ["3 Sáng", "6 Sáng", "9 Sáng", "12 Chiều", "15 Chiều", "18 Chiều", "21 Chiều", "24 Chiều"],
            series: [data_ajax.TotalAmount]
        };
        var options = {
            height: 300,
            axisY: {
                labelInterpolationFnc: function (value) {
                    return (value / 1000000) + 'tr';
                }
            }
        }
        // In the global name space Chartist we call the Bar function to initialize a bar chart. As a first parameter we pass in a selector where we would like to get our chart created and as a second parameter we pass our data object.
        new Chartist.Bar('#my-chart', data, options);
    });
    $("#btn-tab2").click(function () {
        $.get("/Admin/dashboard/lastday", {
        }, function (data_ajax) {
            $("#listsptab2").empty();
            for (i = 0; i < data_ajax.spbanchaydata.length; i++) {
                var j = i + 1;
                $("#listsptab2").append("<div class='sp-banchay-item'><div class='dashboard-spbanchay-left'><img src='" + data_ajax.spbanchaydata[i].image + "'/><span> " + j + " </span></div><div class='dashboard-spbanchay-right'>" + data_ajax.spbanchaydata[i].Name + "</a><br />(Đã bán " + data_ajax.spbanchaydata[i].quantity + " sản phẩm)</div>")
            }
            var data = {
                labels: ["3 Sáng", "6 Sáng", "9 Sáng", "12 Chiều", "15 Chiều", "18 Chiều", "21 Chiều", "24 Chiều"],
                series: [data_ajax.TotalAmount]
            };
            var options = {
                height: 300,
                axisY: {
                    labelInterpolationFnc: function (value) {
                        return (value / 1000000) + 'tr';
                    }
                }
            }


            // In the global name space Chartist we call the Bar function to initialize a bar chart. As a first parameter we pass in a selector where we would like to get our chart created and as a second parameter we pass our data object.
            new Chartist.Bar('#my-chart1', data, options);
        });
    });
    $("#btn-tab3").click(function () {
        $.get("/Admin/dashboard/sevendays", {
        }, function (data_ajax) {
            $("#listsptab3").empty();
            for (i = 0; i < data_ajax.spbanchaydata.length; i++) {
                var j = i + 1;
                $("#listsptab3").append("<div class='sp-banchay-item'><div class='dashboard-spbanchay-left'><img src='" + data_ajax.spbanchaydata[i].image + "'/><span> " + j + " </span></div><div class='dashboard-spbanchay-right'>" + data_ajax.spbanchaydata[i].Name + "</a><br />(Đã bán " + data_ajax.spbanchaydata[i].quantity + " sản phẩm)</div>")
            }
            var data = {
                labels: data_ajax.showdays,
                series: [data_ajax.TotalAmount]
            };
            var options = {
                height: 300,
                axisY: {
                    labelInterpolationFnc: function (value) {
                        return (value / 1000000) + 'tr';
                    }
                }
            }


            // In the global name space Chartist we call the Bar function to initialize a bar chart. As a first parameter we pass in a selector where we would like to get our chart created and as a second parameter we pass our data object.
            new Chartist.Bar('#my-chart2', data, options);
        });
    });
    $("#btn-tab4").click(function () {
        $.get("/Admin/dashboard/month1", {
        }, function (data_ajax) {
            $("#listsptab4").empty();
            for (i = 0; i < data_ajax.spbanchaydata.length; i++) {
                var j = i + 1;
                $("#listsptab4").append("<div class='sp-banchay-item'><div class='dashboard-spbanchay-left'><img src='" + data_ajax.spbanchaydata[i].image + "'/><span> " + j + " </span></div><div class='dashboard-spbanchay-right'>" + data_ajax.spbanchaydata[i].Name + "</a><br />(Đã bán " + data_ajax.spbanchaydata[i].quantity + " sản phẩm)</div>")
            }
            var data = {
                labels: data_ajax.showdays,
                series: [data_ajax.TotalAmount]
            };
            var options = {
                height: 300,
                axisY: {
                    labelInterpolationFnc: function (value) {
                        return (value / 1000000) + 'tr';
                    }
                }
            }


            // In the global name space Chartist we call the Bar function to initialize a bar chart. As a first parameter we pass in a selector where we would like to get our chart created and as a second parameter we pass our data object.
            new Chartist.Bar('#my-chart3', data, options);
        });
    });
    $("#btn-tab5").click(function () {
        $.get("/Admin/dashboard/month2", {
        }, function (data_ajax) {
            $("#listsptab5").empty();
            for (i = 0; i < data_ajax.spbanchaydata.length; i++) {
                var j = i + 1;
                $("#listsptab5").append("<div class='sp-banchay-item'><div class='dashboard-spbanchay-left'><img src='" + data_ajax.spbanchaydata[i].image + "'/><span> " + j + " </span></div><div class='dashboard-spbanchay-right'>" + data_ajax.spbanchaydata[i].Name + "</a><br />(Đã bán " + data_ajax.spbanchaydata[i].quantity + " sản phẩm)</div>")
            }
            var data = {
                labels: data_ajax.showdays,
                series: [data_ajax.TotalAmount]
            };
            var options = {
                height: 300,
                axisY: {
                    labelInterpolationFnc: function (value) {
                        return (value / 1000000) + 'tr';
                    }
                }
            }


            // In the global name space Chartist we call the Bar function to initialize a bar chart. As a first parameter we pass in a selector where we would like to get our chart created and as a second parameter we pass our data object.
            new Chartist.Bar('#my-chart4', data, options);
        });
    });
});