﻿//Xử lý menu
$(".page-sidebar-menu .nav-item[data-id='khuyen-mai']").addClass("active").addClass("open");
$(".page-sidebar-menu .nav-item[data-id='khuyen-mai'] > a > .arrow").addClass("open");
$(".page-sidebar-menu .nav-item[data-id='coupon-giam-gia']").addClass("active").addClass("open");

var FormValidation = function () {
    // validation using icons
    var handleValidation2 = function () {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form2 = $("#myform");
        var error2 = $(".alert-danger", form2);
        var success2 = $(".alert-success", form2);

        form2.validate({
            errorElement: "span", //default input error message container
            errorClass: "help-block help-block-error", // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                name: {
                    required: true
                    //remote: {
                    //    url: form2.find("#name").attr("data-val-remote-url"),
                    //    data: {
                    //        current_name: form2.find("#current_name").val(),
                    //    }
                    //}
                }

            },
            messages: {
                name: {
                    required: "Bạn chưa điền tên tin tức",
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit 
                var htmlerror = "";
                $(validator.errorList).each(function (index, value) {
                    htmlerror += "<li>" + value.message + "</li>";
                });
                error2.find("ul").html(htmlerror);
                success2.hide();
                error2.show();
                App.scrollTo(error2, -200);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");
                icon.attr("data-original-title", error.text()).tooltip({ 'container': 'body' });
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest(".form-group").removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight

            },

            success: function (label, element) {
                var icon = $(element).parent(".input-icon").children('i');
                $(element).closest(".form-group").removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },

            submitHandler: function (form) {
                //success2.show();
                error2.hide();
                //form[0].submit();
                $.post(form.action, $(form).serialize(), function (response) {
                    if (response.Code === 200) {
                        alert("Cập nhật dữ liệu thành công");
                        window.location.reload();
                    }
                    else {
                        var htmlerror = "<li>" + response.Msg + "</li>";
                        error2.find("ul").html(htmlerror);
                        success2.hide();
                        error2.show();
                    }
                });
            }
        });


    }


    return {
        //main function to initiate the module
        init: function () {
            handleValidation2();
        }

    };

}();

jQuery(document).ready(function () {
    FormValidation.init();
    $(".form_datetime").datetimepicker({ format: 'dd/mm/yyyy', todayBtn: true, maxView: 4, minView: 2});

    $("#BalanceTypeId").change(function () {
        if ($(this).val() != 0) {
            $.ajax({
                url: "/admin/FinanceHistory/GetBalanceCategory",
                dataType: "json",
                method: "POST",
                data: { id: $(this).val() },
                success: function (response) {
                    $(".detailifo").show();
                    var html =
                        "<i class=\"fa\"></i><select class=\"form-control\" data-val=\"true\" data-val-number=\"The field Hạng mục thu chi must be a number.\" data-val-required=\"The Hạng mục thu chi field is required.\" id=\"BalanceCategoryId\" name=\"BalanceCategoryId\" aria-invalid=\"false\">";
                    for (var item of response) {
                        html += "<option value=\"" + item.value + "\">" + item.text + "</option>";
                    }
                    html += "</select>";
                    $(".ballanceaj").html(html);
                },
                beforeSend: function () {

                },
                error: function () {
                    alert("Không tải được hạng mục thu chi");
                }
            });
        }
       
    });


});

