﻿//Xử lý menu
$(".page-sidebar-menu .nav-item[data-id='cau-hinh-van-chuyen']").addClass("active").addClass("open");
$(".page-sidebar-menu .nav-item[data-id='cau-hinh-van-chuyen'] > a > .arrow").addClass("open");
$(".page-sidebar-menu .nav-item[data-id='phi-ship-tinh-thanh']").addClass("active").addClass("open");

// validation using icons
var handleValidationte = function () {
    var form2 = $('.add-area-form');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);

    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {

        },
        messages: {

        },

        invalidHandler: function (event, validator) { //display error alert on form submit
            var htmlerror = "";
            $(validator.errorList).each(function (index, value) {
                htmlerror += "<li>" + value.message + "</li>";
            });
            error2.find("ul").html(htmlerror);
            success2.hide();
            error2.show();
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            //$(element).removeClass("success").addClass("error");
        },

        highlight: function (element) { // hightlight error inputs
            $(element).closest('.item-group').removeClass("success").addClass("error");
        },

        unhighlight: function (element) { // revert the change done by hightlight

        },

        success: function (label, element) {
            $(element).closest('.item-group').removeClass("error").addClass("success");
        },

        submitHandler: function (form) {
            //success2.show();
            error2.hide();
            $.post(form.action, $(form).serialize(), function (response) {
                if (response.Code === 200) {
                    alert("Thêm dữ liệu thành công");
                    window.location.reload();
                }
                else {
                    var htmlerror = "<li>" + response.Msg + "</li>";
                    error2.find("ul").html(htmlerror);
                    success2.hide();
                    error2.show();
                }
            });
        }
    });
}

// validation using icons
var handleEditShipping = function () {
    var form2 = $('.edit-area-form');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);

    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {

        },
        messages: {

        },

        invalidHandler: function (event, validator) { //display error alert on form submit
            var htmlerror = "";
            $(validator.errorList).each(function (index, value) {
                htmlerror += "<li>" + value.message + "</li>";
            });
            error2.find("ul").html(htmlerror);
            success2.hide();
            error2.show();
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            //$(element).removeClass("success").addClass("error");
        },

        highlight: function (element) { // hightlight error inputs
            $(element).closest('.item-group').removeClass("success").addClass("error");
        },

        unhighlight: function (element) { // revert the change done by hightlight

        },

        success: function (label, element) {
            $(element).closest('.item-group').removeClass("error").addClass("success");
        },

        submitHandler: function (form) {
            //success2.show();
            error2.hide();
            $.post(form.action, $(form).serialize(), function (response) {
                if (response.Code === 200) {
                    alert("Cập nhật dữ liệu thành công");
                    window.location.reload();
                }
                else {
                    var htmlerror = "<li>" + response.Msg + "</li>";
                    error2.find("ul").html(htmlerror);
                    success2.hide();
                    error2.show();
                }
            });
        }
    });
}

function showAddAreaForm() {
    $("#ajax-modal").modal("show");
    $.get("/admin/shipping/AddArea",
        function(data) {
            $("#ajax-modal .modal-body").html(data);
            handleValidationte();
        });
    
}
function showUpdateAreaForm(id) {
    $("#ajax-modal").modal("show");
    $.get("/admin/shipping/UpdateArea?id="+id,
        function (data) {
            $("#ajax-modal .modal-body").html(data);
            handleEditShipping();
        });

}
