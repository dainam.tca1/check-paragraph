//Xử lý menu
$(".page-sidebar-menu .nav-item[data-id='san-pham']").addClass("active").addClass("open");
$(".page-sidebar-menu .nav-item[data-id='san-pham'] > a > .arrow").addClass("open");
$(".page-sidebar-menu .nav-item[data-id='danh-sach-nhan']").addClass("active").addClass("open");

function ajaxfilter() {
    if ($(".filter-is .active").val()) {
        $(".tag-item[data-id='" + $(".filter-is .active").attr("data-id") + "']").remove();
        var html = "<div class=\"tag-item\" onclick=\"$(this).remove();SearchItem();\" data-id=\"" + $(".filter-is .active").attr("data-id") + "\" data-value=\"" + $(".filter-is .active").val() + "\"><span>" + $(".filter-is .active").attr("data-name") + ": " + $(".filter-is .active").val() + " <i class=\"fa fa-times\"></i></span></div>";
        $(".tag-list").append(html);
        SearchItem();
        $(".filter-option").fadeToggle();
    }
}
function SearchItem() {
    $.ajax({
        url: '/admin/productlabel/Search',
        type: 'GET',
        data: {
            title: $(".tag-item[data-id='Title']").attr("data-value"),
            pagesize: $('#pagesize').val()
        },
        success: function (result) {
            $('.Ajax-Table').html(result);
        },
    });
}

function EditList() {
    var model = [];
    $(".Ajax-Table input[name='Id']").each(function (index, value) {
        model.push({
            Id: parseInt($(value).val()),
            Name: $(value).parent().parent().find("input[name='Name']").val(),
            Value: $(value).parent().parent().find("input[name='Value']").val()
        });
    });
    $.post(
        "/admin/productlabel/UpdateList",
        { model: model },
        function (result) {
            alert(result.Msg);
            window.location.reload();
        }
    );
}

var FormValidation = function () {

    // validation using icons
    var handleValidation2 = function () {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form2 = $('#myform');
        var error2 = $('.alert-danger', form2);
        var success2 = $('.alert-success', form2);

        form2.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                name: {
                    required: true

                }

            },
            messages: {
                name: {
                    required: "Bạn chưa điền tên thương hiệu",
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit 
                var htmlerror = "";
                $(validator.errorList).each(function (index, value) {
                    htmlerror += "<li>" + value.message + "</li>";
                });
                error2.find("ul").html(htmlerror);
                success2.hide();
                error2.show();
                App.scrollTo(error2, -200);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");
                icon.attr("data-original-title", error.text()).tooltip({ 'container': 'body' });
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight

            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },

            submitHandler: function (form) {
                //success2.show();
                error2.hide();
                //form[0].submit();
                //for (instance in CKEDITOR.instances)
                //    CKEDITOR.instances[instance].updateElement();
                Pace.track(function () {
                    $.post(form.action, $(form).serialize(), function (response) {
                        if (response.Code === 200) {
                            alert("Cập nhật dữ liệu thành công");
                            window.location.reload();
                        }
                        else {
                            var htmlerror = "<li>" + response.Msg + "</li>";
                            error2.find("ul").html(htmlerror);
                            success2.hide();
                            error2.show();
                        }
                    });
                });
            }
        });


    }


    return {
        //main function to initiate the module
        init: function () {
            handleValidation2();
        }

    };

}();

jQuery(document).ready(function () {
    FormValidation.init();
});
