﻿var FormValidation = (function() {
  // validation using icons
  var handleValidation2 = function() {
    // for more info visit the official plugin documentation:
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $(".login-form");
    var error2 = $(".alert-danger", form2);
    var success2 = $(".alert-success", form2);

    form2.validate({
      errorElement: "span", //default input error message container
      errorClass: "help-block help-block-error", // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: "", // validate all fields including form hidden input
      rules: {},
      messages: {},

      invalidHandler: function(event, validator) {
        //display error alert on form submit
        var htmlerror = "";
        $(validator.errorList).each(function(index, value) {
          htmlerror += "<li>" + value.message + "</li>";
        });
        error2.find("ul").html(htmlerror);
        success2.hide();
        error2.show();
        App.scrollTo(error2, -200);
      },

      errorPlacement: function(error, element) {
        // render error placement for each input type
        var icon = $(element)
          .parent(".input-icon")
          .children("i");
        icon.removeClass("fa-check").addClass("fa-warning");
        icon
          .attr("data-original-title", error.text())
          .tooltip({ container: "body" });
      },

      highlight: function(element) {
        // hightlight error inputs
        $(element)
          .closest(".form-group")
          .removeClass("has-success")
          .addClass("has-error"); // set error class to the control group
      },

      unhighlight: function(element) {
        // revert the change done by hightlight
      },

      success: function(label, element) {
        var icon = $(element)
          .parent(".input-icon")
          .children("i");
        $(element)
          .closest(".form-group")
          .removeClass("has-error")
          .addClass("has-success"); // set success class to the control group
        icon.removeClass("fa-warning").addClass("fa-check");
      },

      submitHandler: function(form) {
        //success2.show();
        error2.hide();
        //form[0].submit();
        $.post(form.action, $(form).serialize(), function(response) {
          if (response.status == "error") {
            $(form2)
              .find(".error-lst")
              .css("display", "block");
            $(form2)
              .find(".success-lst")
              .css("display", "none");
            $(form2)
              .find(".error-lst ul")
              .html("<li>" + response.messages + "</li>");
          } else {
            $(form2)
              .find(".error-lst")
              .css("display", "none");
            $(form2)
              .find(".success-lst")
              .css("display", "block");
            $(form2)
              .find(".success-lst ul")
              .html("<li>Login Success</li>");
            window.location.href = "/Admin/Dashboard";
          }
        });
      }
    });
  };

  // validation using icons
  var handleValidation3 = function() {
    // for more info visit the official plugin documentation:
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $(".forget-form");
    var error2 = $(".alert-danger", form2);
    var success2 = $(".alert-success", form2);

    form2.validate({
      errorElement: "span", //default input error message container
      errorClass: "help-block help-block-error", // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: "", // validate all fields including form hidden input
      rules: {},
      messages: {},

      invalidHandler: function(event, validator) {
        //display error alert on form submit
        var htmlerror = "";
        $(validator.errorList).each(function(index, value) {
          htmlerror += "<li>" + value.message + "</li>";
        });
        error2.find("ul").html(htmlerror);
        success2.hide();
        error2.show();
        App.scrollTo(error2, -200);
      },

      errorPlacement: function(error, element) {
        // render error placement for each input type
        var icon = $(element)
          .parent(".input-icon")
          .children("i");
        icon.removeClass("fa-check").addClass("fa-warning");
        icon
          .attr("data-original-title", error.text())
          .tooltip({ container: "body" });
      },

      highlight: function(element) {
        // hightlight error inputs
        $(element)
          .closest(".form-group")
          .removeClass("has-success")
          .addClass("has-error"); // set error class to the control group
      },

      unhighlight: function(element) {
        // revert the change done by hightlight
      },

      success: function(label, element) {
        var icon = $(element)
          .parent(".input-icon")
          .children("i");
        $(element)
          .closest(".form-group")
          .removeClass("has-error")
          .addClass("has-success"); // set success class to the control group
        icon.removeClass("fa-warning").addClass("fa-check");
      },

      submitHandler: function(form) {
        //success2.show();
        error2.hide();
        //form[0].submit();
        $.post(form.action, $(form).serialize(), function(response) {
          var htmlerror = "<li>" + response.Msg + "</li>";
          error2.find("ul").html(htmlerror);
          success2.hide();
          error2.show();
        });
      }
    });
  };

  var changePass = function() {
    // for more info visit the official plugin documentation:
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $(".changepass-form");
    var error2 = $(".alert-danger", form2);
    var success2 = $(".alert-success", form2);

    form2.validate({
      errorElement: "span", //default input error message container
      errorClass: "help-block help-block-error", // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: "", // validate all fields including form hidden input
      rules: {},
      messages: {},

      invalidHandler: function(event, validator) {
        //display error alert on form submit
        var htmlerror = "";
        $(validator.errorList).each(function(index, value) {
          htmlerror += "<li>" + value.message + "</li>";
        });
        error2.find("ul").html(htmlerror);
        success2.hide();
        error2.show();
        App.scrollTo(error2, -200);
      },

      errorPlacement: function(error, element) {
        // render error placement for each input type
        var icon = $(element)
          .parent(".input-icon")
          .children("i");
        icon.removeClass("fa-check").addClass("fa-warning");
        icon
          .attr("data-original-title", error.text())
          .tooltip({ container: "body" });
      },

      highlight: function(element) {
        // hightlight error inputs
        $(element)
          .closest(".form-group")
          .removeClass("has-success")
          .addClass("has-error"); // set error class to the control group
      },

      unhighlight: function(element) {
        // revert the change done by hightlight
      },

      success: function(label, element) {
        var icon = $(element)
          .parent(".input-icon")
          .children("i");
        $(element)
          .closest(".form-group")
          .removeClass("has-error")
          .addClass("has-success"); // set success class to the control group
        icon.removeClass("fa-warning").addClass("fa-check");
      },

      submitHandler: function(form) {
        //success2.show();
        error2.hide();
        //form[0].submit();
        $.post(form.action, $(form).serialize(), function(response) {
          if (!response.Ok) {
            var htmlerror = "<li>" + response.Msg + "</li>";
            error2.find("ul").html(htmlerror);
            success2.hide();
            error2.show();
          } else {
            document.location = "/admin/dashboard";
          }
        });
      }
    });
  };

  return {
    //main function to initiate the module
    init: function() {
      handleValidation2();
      handleValidation3();
      changePass();
    }
  };
})();

jQuery(document).ready(function() {
  FormValidation.init();
});

jQuery("#forget-password").click(function() {
  jQuery(".login-form").hide();
  jQuery(".forget-form").show();
});

jQuery("#back-btn").click(function() {
  jQuery(".login-form").show();
  jQuery(".forget-form").hide();
});
var loading;
function changePassword(obj) {
  var target = $(obj).attr("id");
  $.ajax({
    url: $("#" + target).attr("action"),
    dataType: "json",
    method: "POST",
    data: $(obj).serialize(),
    success: function(response) {
      loading = false;
      if (response.status == "error") {
        $("#" + target)
          .find(".error-lst")
          .css("display", "block");
        $("#" + target)
          .find(".success-lst")
          .css("display", "none");
        $("#" + target)
          .find(".error-lst ul")
          .html("<li>" + response.message + "</li>");
      } else {
        $("#" + target)
          .find(".error-lst")
          .css("display", "none");
        $("#" + target)
          .find(".success-lst")
          .css("display", "block");
        $("#" + target)
          .find(".success-lst ul")
          .html("<li>" + response.message + "</li>");
      }
    },
    beforeSend: function() {
      loading = true;
    },
    error: function() {
      loading = false;
    }
  });
  return false;
}
