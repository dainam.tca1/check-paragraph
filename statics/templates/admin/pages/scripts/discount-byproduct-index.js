//Xử lý menu
$(".page-sidebar-menu .nav-item[data-id='khuyen-mai']").addClass("active").addClass("open");
$(".page-sidebar-menu .nav-item[data-id='khuyen-mai'] > a > .arrow").addClass("open");
$(".page-sidebar-menu .nav-item[data-id='discount-by-product']").addClass("active").addClass("open");

function ajaxfilter() {
    if ($(".filter-is .active").val()) {
        $(".tag-item[data-id='" + $(".filter-is .active").attr("data-id") + "']").remove();
        var html = "<div class=\"tag-item\" onclick=\"$(this).remove();SearchItem();\" data-id=\"" + $(".filter-is .active").attr("data-id") + "\" data-value=\"" + $(".filter-is .active").val() + "\"><span>" + $(".filter-is .active").attr("data-name") + ": " + $(".filter-is .active").val() + " <i class=\"fa fa-times\"></i></span></div>";
        $(".tag-list").append(html);
        SearchItem();
        $(".filter-option").fadeToggle();
    }
}
function SearchItem() {
    $.ajax({
        url: '/admin/ProductPromotion/Search',
        type: 'GET',
        data: {
            code: $(".tag-item[data-id='code']").attr("data-value"),
            pagesize: $('#pagesize').val()
        },
        success: function (result) {
            $('.Ajax-Table').html(result);
        }
    });
}
