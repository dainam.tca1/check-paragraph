//Xử lý menu
$(".page-sidebar-menu .nav-item[data-id='don-hang']").addClass("active").addClass("open");

function ajaxfilter() {
    if ($(".filter-is .active").val()) {
        $(".tag-item[data-id='" + $(".filter-is .active").attr("data-id") + "']").remove();
        var html = "<div class=\"tag-item\" onclick=\"$(this).remove();SearchItem();\" data-id=\"" + $(".filter-is .active").attr("data-id") + "\" data-value=\"" + $(".filter-is .active").val() + "\"><span>" + $(".filter-is .active").attr("data-name") + ": " + $(".filter-is .active").val() + " <i class=\"fa fa-times\"></i></span></div>";
        $(".tag-list").append(html);
        SearchItem();
        $(".filter-option").fadeToggle();
    }
}
function SearchItem() {
    $.ajax({
        url: '/admin/order/Search',
        type: 'GET',
        data: {
            orderId: $(".tag-item[data-id='orderId']").attr("data-value"),
            phonenumber: $(".tag-item[data-id='phonenumber']").attr("data-value"),
            status: $(".tag-item[data-id='status']").attr("data-value"),
            pagesize: $('#pagesize').val()
        },
        success: function (result) {
            $('.Ajax-Table').html(result);
        },
    });
}

function EditList() {
    var model = [];
    $(".Ajax-Table input.item-id").each(function (index, value) {
        model.push({
            Id: parseInt($(value).val()),
            Status: parseInt($(value).parent().parent().find("select.item-status").val())
        });
    });
    $.post(
        "/admin/order/UpdateList",
        { model: model },
        function (result) {
            alert(result.Msg);
            window.location.reload();
        }
    );
}
