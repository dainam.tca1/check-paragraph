//Xử lý menu
$(".page-sidebar-menu .nav-item[data-id='lien-he']").addClass("active").addClass("open");
$(".page-sidebar-menu .nav-item[data-id='lien-he'] > a > .arrow").addClass("open");

function ajaxfilter() {
    if ($(".filter-is .active").val()) {
        $(".tag-item[data-id='" + $(".filter-is .active").attr("data-id") + "']").remove();
        var html = "<div class=\"tag-item\" onclick=\"$(this).remove();SearchItem();\" data-id=\"" + $(".filter-is .active").attr("data-id") + "\" data-value=\"" + $(".filter-is .active").val() + "\"><span>" + $(".filter-is .active").attr("data-name") + ": " + $(".filter-is .active").val() + " <i class=\"fa fa-times\"></i></span></div>";
        $(".tag-list").append(html);
        SearchItem();
        $(".filter-option").fadeToggle();
    }
}
function SearchItem() {
    $.ajax({
        url: '/admin/contact/Search',
        type: 'GET',
        data: {
            name: $(".tag-item[data-id='name']").attr("data-value"),
            phonenumber: $(".tag-item[data-id='phonenumber']").attr("data-value"),
            email: $(".tag-item[data-id='email']").attr("data-value"),
            status: $(".tag-item[data-id='status']").attr("data-value"),
            pagesize: $('#pagesize').val()
        },
        success: function (result) {
            $('.Ajax-Table').html(result);
        },
    });
}

