﻿$(document).ajaxStart(function () { Pace.restart(); });

//Xóa nhiều dữ liệu
function multibledel(url) {
    var checkboxes = document.getElementsByName("lstdel");
    var lst = new Array();
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        if (checkboxes[i].checked) {
            lst.push(checkboxes[i].value);
        }
    }
    if (confirm("Dữ liệu bị xóa không thể phục hồi, bạn có muốn xóa?") == true) {
        $.ajax({
            url: url,
            type: 'POST',
            data: { model: lst },
            success: function (result) {
                alert(result.Msg);
                window.location.reload();
            }
        });
    }
}


function cleanUnicode(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|\;|\||\{|\}|\~|\“|\”|\™|\–|\-/g, "-");
    str = str.replace(/^\-+|\-+$/g, "");
    str = str.replace(/\\/g, "");
    str = str.replace(/-+-/g, "-");
    return str;
}


//#region Multible choice
function toggle(source) {
    checkboxes = document.getElementsByName('lstdel');
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        checkboxes[i].checked = source.checked;
    }
}

function chonnhieucbox(source, groupname) {
    checkboxes = document.getElementsByName(groupname);
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        checkboxes[i].checked = source.checked;
    }
}
//#endregion


$(document).ready(function () {
    //Phan trang ajax
    $(document).on("click", "#nav_grid a[href]", function () {
        $.ajax({
            url: $(this).attr("href"),
            type: 'GET',
            success: function (result) {
                $('.Ajax-Table').html(result);
                $('html, body').animate({
                    scrollTop: $(".Ajax-Table").offset().top
                }, 500);
            }
        });
        return false;
    });

    //Start lọc danh sách
    $('.filter-tool').change(function () {
        $('.filter-is-item').removeClass("active");
        $(".filter-is-item[data-id='" + $(this).val() + "']").addClass("active");
    });
    $(".filter-btn").click(function () {
        $(".filter-option").fadeToggle();
    }); 
});

function logoff() {
    $.post("/CAccount/LogOffAdmin", {}, function () {
        window.location.reload();
    });
}

var timer;
function suggestProductPopup() {
    $.post("/admin/Shared/SuggestProductPopup", {
        productName: $(".suggest-product-popup .suggest-name").val(),
        productCategoryId: $(".suggest-product-popup .suggest-cateId").val(),
        brandId: $(".suggest-product-popup .suggest-brand").val(),
        productCode: $(".suggest-product-popup .suggest-productcode").val()
    }, function (data) {
        $(".suggest-product-popup tbody").html(data);
    });
}

function openSuggestPopup(obj) {
    $(".suggest-product-popup").attr("data-namecheckbox", $(obj).data("namecheckbox")).attr("data-hidden", $(obj).data("hidden")).attr("data-fill", $(obj).data("fill")).modal('show');
}

function GetProFromList() {
    var checkboxes = document.getElementsByName("choosesp");
    var lst = new Array();
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        if (checkboxes[i].checked) {
            lst.push(parseInt(checkboxes[i].value));
        }
    }

    $.ajax({
        url: "/admin/shared/DistinctId",
        type: 'POST',
        data: { checkedList: JSON.stringify(lst), baseList: $($(".suggest-product-popup").attr("data-hidden")).val() },
        success: function (data) {
            if (data.Code === 200) {
                $($(".suggest-product-popup").attr("data-hidden")).val(data.Msg);
                $(".suggest-product-popup").modal('hide');
                FillProductToTable($(".suggest-product-popup").attr("data-hidden"), $(".suggest-product-popup").attr("data-fill"), $(".suggest-product-popup").attr("data-namecheckbox"));
            } else {
                alert(data.Msg);
            }
        }
    });
}

function FillProductToTable(hiddenData, fillData,namecheckbox) {
    $.ajax({
        url: "/admin/shared/FillProductToTable",
        type: 'POST',
        data: { productIdList: $(hiddenData).val(), namecheckbox: namecheckbox },
        success: function (data) {
            if (data.Code === 200) {
                $(fillData).html(data.Msg);
            } else {
                alert(data.Msg);
            }
        }
    });
}

function deletemultipleProduct(obj) {
    var checkboxes = document.getElementsByName($(obj).data("namecheckbox"));
    var lst = new Array();
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        if (checkboxes[i].checked) {
            lst.push(checkboxes[i].value);
        }
    }
    $.ajax({
        url: "/admin/shared/deletemultipleProduct",
        type: 'POST',
        data: { delList: JSON.stringify(lst), productIdList: $($(obj).data("hidden")).val() },
        success: function (data) {
            if (data.Code === 200) {
                $($(obj).data("hidden")).val(data.Msg);
                FillProductToTable($(obj).attr("data-hidden"), $(obj).attr("data-fill"), $(obj).attr("data-namecheckbox"));
            } else {
                alert(data.Msg);
            }
        }
    });
}