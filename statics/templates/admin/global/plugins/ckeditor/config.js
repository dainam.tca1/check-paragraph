/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function(config) {
  // Define changes to default configuration here. For example:
  // config.language = 'fr';wysiwyg
  // config.uiColor = '#AADC6E';
  config.language = "vi";
  config.height = 300;
  config.toolbarCanCollapse = true;
  config.startupMode = "wysiwyg";
  config.htmlEncodeOutput = false;
  config.entities = false;
  config.removeButtons = "Source";

  config.fillEmptyBlocks = false;
  config.tabSpaces = 0;
  config.forcePasteAsPlainText = true;

  config.enterMode = CKEDITOR.ENTER_P;
  config.toolbar = "Full";

  // config.filebrowserBrowseUrl =
  //   "/statics/templates/admin/global/plugins/ckfinder/ckfinder.html";
  // config.filebrowserImageBrowseUrl =
  //   "/statics/templates/admin/global/plugins/ckfinder/ckfinder.html?type=Images";
  // config.filebrowserFlashBrowseUrl =
  //   "/statics/templates/admin/global/plugins/ckfinder/ckfinder.html?type=Flash";
  // config.filebrowserUploadUrl =
  //   "/wwwroot/templates/admin/global/plugins/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files";
  config.filebrowserImageUploadUrl = "/admin/uploadimageck";
  // config.filebrowserFlashUploadUrl =
  //   "/wwwroot/templates/admin/global/plugins/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash";
  config.filebrowserWindowWidth = "1000";
  config.filebrowserWindowHeight = "700";

  config.extraPlugins = "youtube,sourcedialog";
  config.allowedContent = true;
  config.extraAllowedContent =
    "p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*}";
  config.image_prefillDimensions = false;
  CKEDITOR.dtd.$removeEmpty.span = false;
  CKEDITOR.dtd.$removeEmpty.i = false;
};

CKEDITOR.on("instanceReady", function(ev) {
  ev.editor.on(
    "paste",
    function(evt) {
      //   //   evt.data.dataValue = evt.data.dataValue.replace(/&nbsp;/g, "");
      //   //   evt.data.dataValue = evt.data.dataValue.replace(/<p><\/p>/g, "");
      //   console.log(evt.data.dataValue);
    },
    null,
    null,
    9
  );
});
