// server.js
import path from "path";
// CẤU HÌNH DATABASE
// =============================================================================
// CẤU HÌNH EXPRESS JS
// =============================================================================
import express from "express";
import hbs from "express-handlebars";

//#region CRON JOB WORKING HERE
//#endregion
const port = 3000; //process.env.PORT ||
const app = express();
// =============================================================================
//#region PASSPORT
// require("./configs/passport")(passport); // pass passport for configuration
//#endregion
// =============================================================================
//#region SET UP EXPRESS APPLICATION
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
//#endregion
// =============================================================================
// required for passport
app.use(cookieParser()); // đọc cookies (dùng cho authen)
//app.use(morgan("dev")); // log every request to the console

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());

hbs
  .create()
  .getPartials()
  .then(function(partials) {});
// =============================================================================
app.engine(
  "hbs",
  hbs({
    extname: "hbs",
    defaultLayout: "_layout",
    layoutsDir: path.join(__dirname, "/"),
    partialsDir: path.join(
      __dirname,
      `templates/views/shared/`
    ),
  })
);
app.set("view engine", "hbs");
app.set("views", path.join(__dirname, "/"));
app.use("/statics", express.static(path.join(__dirname, "statics")));
app.use("/sitemap.xml", express.static(path.join(__dirname, "/sitemap.xml")));
app.use("/robot.txt", express.static(path.join(__dirname, "/robot.txt")));
app.use(
  "/templates/statics",
  express.static(path.join(__dirname, "templates/statics"))
);
// CẤU HÌNH ROUTES
// =============================================================================
//#region API Local

//#endregion


//#region Router Local

import CheckParagraphRouter from "./modules/checkparagraph-module/routes/controller-router";
//#endregion

// Middleware
// router.use(function(req, res, next) {
//   // do logging
//   console.log("Something is happening.");
//   next(); // make sure we go to the next routes and don't stop here
// });
//#region Cấu hình route cho api
//#region Cấu hình route cho controller
app.use("/", CheckParagraphRouter);
//#endregion




// START THE SERVER
// =============================================================================
app.listen(port);
