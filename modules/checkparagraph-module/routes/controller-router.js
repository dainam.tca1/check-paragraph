// Initialize express router
import express from "express";
const router = express.Router();
// Set default API response
// Importcontroller
import Controller from "../controllers/controller";
router.route("/").get(Controller.index);
// Export API routes
module.exports = router;
