import path from "path";
exports.index = async (req, res) => {
  try {
    //Get shopping cart
    res.render(
      path.join(
        `templates/views/index/index.hbs`
      ),
      {
        layout: false
      }
    );
    return true;
  } catch (error) {
    res.sendStatus(404);
    return false;
  }
};

//#endregion
