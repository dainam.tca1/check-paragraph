import React, { Component } from "react";
import { BrowserRouter, Redirect, Route } from "react-router-dom";

import CheckParagraph from "./index.jsx";
class App extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <Route exact path="/" component={CheckParagraph} />
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

export default App;
