import React, { Component } from "react";
//ckeditor
import CkEditor from "../components/ckeditor.jsx";

class CheckParagraph extends Component {
  constructor() {
    super();
    var that = this;
    that.state = {
      model: {
        PageContent: "",
        Results:""
      }
    };
    that.setState(that.state);
  }
  /**
   * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
   * Filter dữ liệu theo tham số mới
   * @param {Event} event
   */
  
  /**
   * Xóa data đã được chọn
   */
  CheckParagraph(value){
    var that = this;
    if(value == null || value == ""){
      return -1;
    } 
    var check_error_60  = [];
    var check_error_20  = [];
    var arrValue = value.split("</p>");
    arrValue.pop();
    if(arrValue.length <= 0){ return -1}
    arrValue.forEach((e , i) =>{
      var count = that.countWordsUsingSplit(e);
      if(count < 60){
        check_error_20.push(e);
      }else{
        check_error_60.push(e);
      } 
    })
    var arr_text_20 = [];
    check_error_20.forEach((e , i) => {
      var text = e.trim().split(".");
      text.pop();
      text.forEach((c, d) => {
        if(c != "" || c != null){
          if(that.countWordsUsingSplit(c) >= 20){
            arr_text_20.push(c.trim()+".");
          }
        }
      })
    });
    arr_text_20.forEach((e , i ) => {   
      var check_p = e.search("<p>");
      if(check_p >= 0 && !isNaN(check_p)){
        var rm_p_f = e.replace("<p>","");
        if(check_p == 0){ 
          that.state.model.Results = that.state.model.Results.replace(e , "<span style='background:yellow'>"+rm_p_f+"</span>");
          that.setState(that.state);
        }else{
          that.state.model.Results = that.state.model.Results.replace(e , "<p><span style='background:yellow'>"+rm_p_f+"</span></p>");
          that.setState(that.state);
        }
       
      }else{
        var rm_p_f = e.replace("<p>","");
        that.state.model.Results = that.state.model.Results.replace(e , "<span style='background:yellow;display:inline-block'>"+ rm_p_f+"</span>");
        that.setState(that.state);
      }
      
    })
    check_error_60.forEach((e, i) => {
      var add_p  = e+"</p>";
      var rm_p_f = add_p.replace("<p>","");
      var rm_p_l = rm_p_f.replace("</p>","");
      that.state.model.Results  = that.state.model.Results.replace(add_p,"<p style='background:red;color:white;line-height: 24px'>"+rm_p_l+"</p>");
      that.setState(that.state);
    })
    that.setState(that.state);
   
  }
  countWordsUsingSplit(input) {
    if (input == null || input == "") {
      return 0;
    }
    var words = input.split(" ");
    return words.length;
  }
  componentWillMount() {
    //Load script ckeditor lên
    $("#scriptloading").html(
      `<script src="/statics/templates/admin/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
    );
    $("#cssloading").html(
      `<script src="/statics/templates/admin/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
    );
  }
  componentDidMount() {
    //Sửa title trang
  
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
           <div className="container">
               <div className="form-group">
             <span className="required" />
             <div className="input-icon right">
               <i className="fa" />
               <CkEditor
                 id="PageContent"
                 value={this.state.model.PageContent}
                 onChange={e => {
                  this.state.model.PageContent = e;
                  this.state.model.Results = e;
                  this.CheckParagraph(e);
                  this.setState(this.state);
                 }}
                 //onBlur={e =>{
                 // this.state.model.Results = e;
                 // this.CheckParagraph(e);
                // }}
               />
             </div>
           </div>
           <h2>Result</h2>
             <div className="form-group">
             <span className="required" />
             <div className="input-icon right">
               <i className="fa" />
               <div dangerouslySetInnerHTML={{ __html: this.state.model.Results}} />
             </div>
           </div>
           </div>
        ): ""}
      </React.Fragment>
    );
  }
}

export default CheckParagraph;
