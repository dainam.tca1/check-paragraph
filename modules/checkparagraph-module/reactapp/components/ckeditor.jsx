import React, { Component } from "react";

class CkEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editor: null
    };
    this.textinput = React.createRef();
  }
  componentDidMount() {
    var that = this;
    that.state.editor = CKEDITOR.replace(that.textinput.current);
    that.state.editor.on("change", () => {
      that.props.onChange(that.state.editor.getData());
    });
    // that.state.editor.on("blur", () => {
    //   that.props.onBlur(that.state.editor.getData());
    // });
    that.setState(that.state);
  }




  componentWillReceiveProps(nextProps) {
    var that = this;
    if(nextProps.value !== that.props.value){
     //console.log(nextProps.value);
     // that.state.editor.instances["Results"].setData(nextProps.value);
     //that.state.editor.setData(nextProps.value)
     // that.setState(that.state);
    }
  }

  componentWillUnmount() {
    this.state.editor.destroy(true);
  }
  render() {
    return <textarea value={this.props.value} ref={this.textinput} />;
  }
}

export default CkEditor;
