import moment from "moment";
import request from "request";
import util from "util";
import crypto from "crypto";

const POSMENU_CACHE_KEY = "POSMENU_CACHE_KEY";
const POS_ALLCUSTOMER_CACHE_KEY = "POS_ALLCUSTOMER_CACHE_KEY";

const POS_USERNAME = "admin"; //admin admin
const POS_PASSWORD = "6786716888"; //6786716888 6786716888
const POS_DEVICE = "3A-9BE37F1696E28"; //CF4-C5075B7C5F9F4 3A-9BE37F1696E28
const POS_API_URL = "https://dkmobility.gposdev.com/avacafe/api/"; //https://www.gposdev.com/20003/api/  https://dkmobility.gposdev.com/avacafe/api/
const POS_UTC_TIME = -5;

var makePosApiRequest = async (apiname, method, requestbody) => {
  try {
    var basicAuthentication = Buffer.from(
      `${POS_USERNAME}:${POS_PASSWORD}`
    ).toString("base64");
    var tokenString = crypto
      .createHmac("sha1", basicAuthentication)
      .update(moment.utc().format("MM/DD/YYYY"))
      .digest("base64");

    // console.log(requestbody);

    var requestpromise = util.promisify(request);
    var result = await requestpromise(`${POS_API_URL}${apiname}`, {
      method: method,
      headers: {
        Authorization: `Basic ${basicAuthentication}`,
        "Content-Type": "application/json",
        token: tokenString,
        device: POS_DEVICE
      },
      json: requestbody
    });
    return result;
  } catch (error) {
    return null;
  }
};

module.exports = {
  POSMENU_CACHE_KEY,
  POS_ALLCUSTOMER_CACHE_KEY,
  makePosApiRequest,
  POS_UTC_TIME
};
