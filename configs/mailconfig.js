//#region thông tin website EMAIL
const WEBSITE_EMAIL = process.env.WEBSITE_EMAIL;
const WEBSITE_EMAIL_NOTIFICATION = process.env.WEBSITE_EMAIL_NOTIFICATION;
const EMAIL_SEND_USER = "noreply.webnails.money@gmail.com";
const EMAIL_SEND_PASS = "fdtuncdleuchlszc";
const MAIL_HOST = "smtp.gmail.com";
const SEND_MAIL_PORT = "587";
const SEND_MAIL_IS_SECURE = false; // true for port 465, false for other ports
const SEND_MAIL_IS_REJECT_UNAUTHORIZED = false;
//#endregion

module.exports = {
  WEBSITE_EMAIL,
  EMAIL_SEND_USER,
  EMAIL_SEND_PASS,
  WEBSITE_EMAIL_NOTIFICATION,
  MAIL_HOST,
  SEND_MAIL_PORT,
  SEND_MAIL_IS_SECURE,
  SEND_MAIL_IS_REJECT_UNAUTHORIZED
};
