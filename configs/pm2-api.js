const WEBNAILS_USERNAME = "dan@dkmobility.com"; //dan@dkmobility.com // dkmobility //avacafe
const WEBNAILS_PASSWORD = "qJI$J3jE+NA1"; //"qJI$J3jE+NA1" // Thangloi@90! //avacafestreet2019
// const MXMERCHANT_CONSUMERKEY = "DrYxx2yKmbNILXOzyz8uNAT0";
// const MXMERCHANT_CONSUMERSECRET = "6zzVyXfIHZ1buqidShn3vs/nQMg=";
const WEBNAILS_ID = "282064462"; //"282064462" // 418329526 //418419712
const WEBNAILS_API_WEBSERVICE_URL = "/api/webserviceServer/"; //"https://sandbox.api.mxmerchant.com/checkout/v3/" "https://api.mxmerchant.com/checkout/v3/"
const WEBNAILS_API_DIGITALSIGNATURE_URL = "/api/digitalServer/";
const util = require("util");
const request = require("request");

var makeWebsiteApiRequest = async (ipserver,apiname, method, requestbody) => {
  try {
    var basicAuthentication = Buffer.from(
      `${WEBNAILS_USERNAME}:${WEBNAILS_PASSWORD}`
    ).toString("base64");

    var requestpromise = util.promisify(request);
    return await requestpromise(`${ipserver}${WEBNAILS_API_WEBSERVICE_URL}${apiname}`, {
      method: method,
      headers: {
        Authorization: `${basicAuthentication}`,
        "Content-Type": "application/json",
      },
      json: requestbody
    });
  } catch (error) {
    console.log(error);
    return null;
  }
};
var makeDigitalApiRequest = async (ipserver,apiname, method, requestbody) => {
  try {
    var basicAuthentication = Buffer.from(
        `${WEBNAILS_USERNAME}:${WEBNAILS_PASSWORD}`
    ).toString("base64");

    var requestpromise = util.promisify(request);
    return await requestpromise(`${ipserver}${WEBNAILS_API_DIGITALSIGNATURE_URL}${apiname}`, {
      method: method,
      headers: {
        Authorization: `${basicAuthentication}`,
        "Content-Type": "application/json",
      },
      json: requestbody
    });
  } catch (error) {
    console.log(error);
    return null;
  }
};
module.exports = {
  makeWebsiteApiRequest,
  makeDigitalApiRequest,
  WEBNAILS_ID,
  WEBNAILS_USERNAME,
  WEBNAILS_PASSWORD
};
