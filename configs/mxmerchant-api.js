const MXMERCHANT_USERNAME = "dan@dkmobility.com"; //dan@dkmobility.com // dkmobility //avacafe
const MXMERCHANT_PASSWORD = "qJI$J3jE+NA1"; //"qJI$J3jE+NA1" // Thangloi@90! //avacafestreet2019
// const MXMERCHANT_CONSUMERKEY = "DrYxx2yKmbNILXOzyz8uNAT0";
// const MXMERCHANT_CONSUMERSECRET = "6zzVyXfIHZ1buqidShn3vs/nQMg=";
const MXMERCHANT_MERCHANT_ID = "282064462"; //"282064462" // 418329526 //418419712
const MXMERCHANT_API_URL = "https://sandbox.api.mxmerchant.com/checkout/v3/"; //"https://sandbox.api.mxmerchant.com/checkout/v3/" "https://api.mxmerchant.com/checkout/v3/"
const MXMERCHANTRECURRING_API_URL = "https://sandbox.api.mxmerchant.com/checkout/v3/contractsubscription"; //"" ""
const util = require("util");
const request = require("request");

var makeMxmerchantRequest = async (apiname, method, requestbody) => {
  try {
    var basicAuthentication = Buffer.from(
      `${MXMERCHANT_USERNAME}:${MXMERCHANT_PASSWORD}`
    ).toString("base64");

    var requestpromise = util.promisify(request);
    var result = await requestpromise(`${MXMERCHANT_API_URL}${apiname}`, {
      method: method,
      headers: {
        Authorization: `Basic ${basicAuthentication}`,
        "Content-Type": "application/json"
      },
      json: requestbody
    });
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
};
var makeMxmerchantRecurringRequest = async (apiname, method, requestbody) => {
  try {
    var basicAuthentication = Buffer.from(
        `${MXMERCHANT_USERNAME}:${MXMERCHANT_PASSWORD}`
    ).toString("base64");

    var requestpromise = util.promisify(request);
    var result = await requestpromise(`${MXMERCHANTRECURRING_API_URL}${apiname}`, {
      method: method,
      headers: {
        Authorization: `Basic ${basicAuthentication}`,
        "Content-Type": "application/json"
      },
      json: requestbody
    });
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
};
module.exports = {
  makeMxmerchantRequest,
  makeMxmerchantRecurringRequest,
  MXMERCHANT_MERCHANT_ID
};
