import cron from "cron";
import { makeWebsiteApiRequest , makeDigitalApiRequest } from "./pm2-api";
import IpServer from "../modules/websiteservice-module/models/schema/ipserver";
const cronTimeSetting = "0 0 * * *"; // One minute 1 * * * * // 0 0 * * *
//#region CronJob
module.exports.job =  new cron.CronJob({
    cronTime: cronTimeSetting,
    onTick: async ()=>{
       var ip = await GetAllIpServer();
        for(var i = 0; i <= ip.length - 1 ; i++){
            var checkWebsiteService = await makeWebsiteApiRequest(ip[i].Name,'website',"POST",null);
            var checkDigitalSignature = await makeDigitalApiRequest(ip[i].Name,'device',"POST",null);
        }
       },
    start:true,
    timeZone: "America/Kentucky/Monticello"
    }
);
//#endregion
//#region Get IpServer
const GetAllIpServer = async () =>{
    var ip = await IpServer.find({});
    if(!ip){
        return false;
    }
    var IPList = ip.map(e =>{
        return {Name: e.Value}
    });
    return IPList;
} 
//#endregion
