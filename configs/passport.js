import { Strategy as LocalStrategy } from "passport-local";
import { Strategy as FacebookStrategy } from "passport-facebook";
import EcsUser from "../modules/authentication-module/models/schema/ecs-user";

import auth from "./auth";

module.exports = passport => {
  // used to serialize the user for the session
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  // used to deserialize the user
  passport.deserializeUser((id, done) => {
    EcsUser.findById(id, (err, user) => {
      done(err, user);
    });
  });
  // =========================================================================
  // LOCAL LOGIN =============================================================
  // =========================================================================
  // we are using named strategies since we have one for login and one for signup
  // by default, if there was no name, it would just be called 'local'
  passport.use(
    "local-login-member",
    new LocalStrategy(
      {
        // by default, local strategy uses username and password, we will override with email
        usernameField: "phoneoremail",
        passwordField: "password",
        passReqToCallback: true // allows us to pass back the entire request to the callback
      },
      (req, phoneoremail, password, done) => {
        process.nextTick(async () => {
          try {
            var user = await EcsUser.findOne({
              $or: [
                { "Local.Phone": phoneoremail },
                { "Local.Email": phoneoremail }
              ]
            }).exec();
            if (!user) {
              return done(
                new Error(
                  "Wrong password. Please try again or click forgot password."
                )
              );
            }
            if (
              !user.validPassword(req.body.password, user.Local.PasswordHash)
            ) {
              return done(
                new Error(
                  "Wrong password. Please try again or click forgot password."
                )
              );
            }
            if (user.Local.Token) {
              return done(new Error("Please verify your email to continute!"));
            }
            return done(null, user);
          } catch (error) {
            return done(
              new Error(
                "Wrong password. Please try again or click forgot password."
              )
            );
          }
        });
      }
    )
  );
  passport.use(
    "local-login",
    new LocalStrategy(
      {
        // by default, local strategy uses username and password, we will override with email
        usernameField: "phone",
        passwordField: "password",
        passReqToCallback: true // allows us to pass back the entire request to the callback
      },
      (req, phone, password, done) => {
        // we are checking to see if the user trying to login already exists

        EcsUser.findOne({
            $or: [
                { "Local.Phone": phone },
                { "Local.Email": phone }
            ]
        }, function(err, user) {
          // if there are any errors, return the error before anything else
          if (err) return done(err);
          // all is well, return successful user
          return done(null, user);
        });
      }
    )
  );
  // =========================================================================
  // LOCAL SIGNUP ============================================================
  // =========================================================================
  // we are using named strategies since we have one for login and one for signup
  // by default, if there was no name, it would just be called 'local'
  passport.use(
    "local-signup",
    new LocalStrategy(
      {
        // by default, local strategy uses username and password, we will override with email
        usernameField: "email",
        passwordField: "password",
        passReqToCallback: true // allows us to pass back the entire request to the callback
      },
      (req, email, password, done) => {
        // asynchronous
        // User.findOne wont fire unless data is sent back
        process.nextTick(() => {
          // find a user whose email is the same as the forms email
          // we are checking to see if the user trying to login already exists
          EcsUser.findOne({ "Local.Email": email }, (err, user) => {
            // if there are any errors, return the error
            if (err) return done(err);

            // check to see if theres already a user with that email
            if (user) {
              return done(null, false);
            } else {
              // if there is no user with that email
              // create the user
              var newUser = new EcsUser();
              // set the user's local credentials
              newUser.Local.Email = email;
              newUser.Local.Password = EcsUser.generateHash(password);

              // save the user
              newUser.save(err => {
                if (err) throw err;
                return done(null, newUser);
              });
            }
          });
        });
      }
    )
  );
  // =========================================================================
  // FACEBOOK LOGIN =============================================================
  // =========================================================================
  // we are using named strategies since we have one for login and one for signup
  // by default, if there was no name, it would just be called 'facebook'
  passport.use(
    new FacebookStrategy(
      {
        clientID: auth.facebookAuth.clientID,
        clientSecret: auth.facebookAuth.clientSecret,
        callbackURL: auth.facebookAuth.callbackURL,
        profileFields: [
          "id",
          "displayName",
          "email",
          "picture.width(200).height(200).type(normal)",
          "first_name",
          "middle_name",
          "last_name",
          "hometown"
        ]
      },
      (accessToken, refreshToken, profile, done) => {
        // EcsUser.findOrCreate({'Facebook.Token':refreshToken}, function(err, user) {
        //   if (err) { return done(err); }
        //   done(null, user);
        // });
        process.nextTick(async () => {
          await EcsUser.findOne(
            { "Facebook.ProfileId": profile.id },
            (err, user) => {
              if (err) return done(err);
              if (user) return done(null, user);
              else {
                var temp = new EcsUser();
                temp.Facebook.Id = Math.random();
                temp.Facebook.Token = accessToken;
                temp.Facebook.Name = profile.displayName;
                temp.Facebook.Picture = profile.photos[0].value.trim();
                temp.Facebook.Location = profile.provider;
                temp.Facebook.ImgWidth = 200;
                temp.Facebook.ImgHeight = 200;
                temp.Facebook.Email = profile.emails[0].value;
                temp.Facebook.ProfileId = profile.id;
                //console.log(profile.photos.values)
                temp.save(() => {
                  return done(null, profile);
                });
                // if(result) return false;
                // else return done(null,temp)
                //if(result) return done(null,temp)
                //return done(null,temp)
              }
            }
          );
        });
      }
    )
  );
};
