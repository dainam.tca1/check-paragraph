module.exports = [
  {
    entry: {
      app: "./modules/checkparagraph-module/reactapp/app.js"
    },
    output: {
      path: __dirname,
      filename: "./statics/js/check.js"
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        }
      ]
    }
  }
];
